var dataTable, settings;

function draw_users() {
    dataTable = $('#users').DataTable({
        "searching": true,
        "processing": false,
        "serverSide": true,
        "serverMethod": "POST",
        "aoColumns": [
            { "name": "user_id" },
            { "name": "first_name" },
            { "name": "last_name" },
            { "name": "email" },
            { "name": "phone" },
            { "name": "balance" },
            { "name": "status" },

            { "bSortable": false }
        ],


        "fnServerData": function(sSource, aoData, fnCallback, oSettings) {

            $("#users .search").each(function() {
                if ($(this).val() != "") {
                    aoData.push({ "name": $(this).attr("name"), "value": $(this).val() });
                }
            });
            var debug = '';
            var sort = aoData[2].value;
            for (item in sort) {
                debug += item + "=" + sort[item];
            }
            //    var order = aoData[2].value.dir;

            aoData.push({ "name": "sort", "value": sort[0].column });
            aoData.push({ "name": "order", "value": sort[0].dir });

            //console.log(sort);

            settings = oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": get_admin_route("user"),
                "data": aoData,
                "success": function(json) {
                    fnCallback(json);
                }
            });


        },

        ajax: get_admin_route("user") //+ '?first_name=' + $("#search_first_name").val()
    });

}

$("#users .search").on('change input keypress', function() {
    // table.clear().draw();
    // console.log("Search");
    // settings.oApi._fnDraw(oSettings);

    //dataTable.api().draw("full-reset");

    dataTable.ajax.reload();
    dataTable.draw("full-reset");

    //console.log(dataTable.api());

    //   dataTable.clear();


    /*
        dataTable.fnClearTable();
        dataTable.fnDraw();

        
         var oSettings = dataTable.fnSettings();
         dataTable.fnClearTable(this);
         for (var i = 0; i < json.aaData.length; i++) {
             dataTable.oApi._fnAddData(oSettings, json.aaData[i]);
         }
         oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
         dataTable.fnDraw();
         */
});