var dataTable, settings;

function draw_transactions() {
    dataTable = $('#transactions').DataTable({
        "searching": true,
        "processing": true,
        "serverSide": true,
        "serverMethod": "POST",

        "aoColumns": [

            { "name": "transaction_id" },
            { "name": "user" },
            { "name": "amount" },
            { "bSortable": false },
            { "name": "transaction_type" },
            { "name": "transaction_time" },
            { "name": "transaction_status" },
            { "bSortable": false },

            { "bSortable": false }
        ],


        "fnServerData": function(sSource, aoData, fnCallback, oSettings) {

            $("#transactions .search").each(function() {
                if ($(this).val() != "") {
                    aoData.push({ "name": $(this).attr("name"), "value": $(this).val() });
                }
            });

            var sort = aoData[2].value;

            aoData.push({ "name": "sort", "value": sort[0].column });
            aoData.push({ "name": "order", "value": sort[0].dir });

            settings = oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": get_admin_route("transaction"),
                "data": aoData,
                "success": function(json) {
                    fnCallback(json);
                }
            });


        },

        "ajax": get_admin_route("transaction")

    });

}

$("#transactions .search").on('change input keypress', function() {
    // console.log("hi");
    dataTable.ajax.reload();
    dataTable.draw("full-reset");

});