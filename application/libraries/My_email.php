<?php

require_once(BASEPATH . "/libraries/Email.php");

class My_email extends CI_Email {

    //protected $recent = "fkabeer";

    function message($message){
        ob_start();
        $CI = &get_instance();
        $CI->load->view("email/template.phtml", array(
            "message" => $message
        ));
        $body = ob_get_contents();
        ob_clean(); 

        $this->set_mailtype('html');

        parent::message($body);
    }

    function debug(){
      //  echo '<pre>'; print_r($this); echo '</pre>'; 

       echo $this->_body;
    }

    function send($auto_clear = true){

    } 

}


/*
class CoinsTradeEmail extends Email{

    function message($message){
        ob_start();
        $this->load->view("email/template.phtml", array(
            "message" => $message
        ));
        $body = ob_get_contents();
        ob_clean(); 

        parent::message($body);
    }

    function debug(){
        echo '<pre>'; print_r($this); echo '</pre>'; 
    }

}
*/
