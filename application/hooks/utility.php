<?php

function get_root_path(){
    return BASEPATH;
} 

function get_application_path(){
    return APPPATH ;
}

function get_controllers_path(){
    return get_application_path() . "controllers/";
}

function get_models_path($type = NULL){
    return get_application_path() . "models/";
}

function init(){
 //   echo "init"; exit;    
}


function __autoload($class_name){

    $path = get_controllers_path() . $class_name . ".php";

  //  echo $path; exit;

    if(!@include_once(get_controllers_path() . $class_name . ".php")){
        @include_once(get_models_path() . $class_name . ".php");
    }    

}