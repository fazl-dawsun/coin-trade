<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Root extends CI_Controller{

    

    function __construct($invoker){

       
/*
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        ('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
*/
        // date_default_timezone_set("Asia/Karachi");

        parent::__construct();

        $this->load->library('my_email', null, "my_email");

      //  $this->db->query("SET time_zone = 'Asia/Karachi';");        

       
        $this->load->model("Entity/Entity_Draw");
        

        $this->Entity_Draw->run_in_queue();

        $this->db->query("COMMIT");

        $this->load->model("Entity/Entity_Participant");

        $this->Entity_Participant->distribute_prizes();
        
    }
}