<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Content-Type: application/json");
echo json_encode(array(
	"status" => 0,
	"error_type"=> "database", 
	"heading" => $heading,
	"message" => $message
));
exit;

 