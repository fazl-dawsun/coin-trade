<?php 

defined("BASEPATH") OR exit("No direct script access allowed");

class Draw extends Front{

    function __construct($invoker = NULL){
        parent::__construct((is_object($invoker)) ? $invoker : $this);
        auth_access_only();
    }

    function index(){

        $errors = array();       

        $this->load->model("Entity/Entity_Draw");

        //$draws = $this->Entity_Draw->fetch_bulk();

        $this->push(array(
            "status" => "1",            
            "draws" => $draws
        ));

    }

    function last_draws(){
        //$this->db->select("CONCAT(draw_number, lucky_number) as draw_number, draw_time")->from("draws")->order_by("draw_id", "desc")->limit(10);

        $query = "SELECT CONCAT(draw_number, lucky_number) as draw_number, draw_time FROM draws 
                            WHERE draw_number IS NOT NULL ORDER BY draw_id DESC LIMIT 10 ";

        $draws = $this->db->query($query)->result();

        $this->push(array(
            "status" => 1,
            "draws" => $draws
        ));
    }

    function add(){

        $errors = array();
            $this->load->model("Entity/Entity_Draw");			
			$data["draw_time"] = $this->input->post("draw_time");			

			$draw_id = $this->Entity_Draw->add($data);

            if(!$draw_id){
                $this->load->model("Message/Message_Error");
                $errors = $this->Message_Error->get_by_entity("draws");            
                foreach($errors as $messages){
                    $message .= implode("\n", $messages);
                }
                $this->push(array(
                    "status" => 0,
                    "message" => $message,
                    "draw_id" => 0
                ));
            }
            else {
                $this->push(array(
                    "status" => 1,
                    "message" => "Draw saved, but not executed",
                    "draw_id" => $draw_id
                ));
            }              

    }

    function debug_run(){

        $draw_id = $this->input->post("draw_id");        

        $this->load->model("Entity/Entity_Draw");

        $this->Entity_Draw->run(array(
            "draw_id" => $draw_id
        ));

        $this->db->select("*")->from("draws")->where("draw_id", $draw_id);
        $draw = $this->db->get()->row();

        echo json_encode(array(
            "draw" =>$draw
        ));
        exit;

    }

    function run(){

       $draw_time = $draw_number = NULL;

        $errors = array();
            $this->load->model("Entity/Entity_Draw");		

            $data["draw_id"] = $this->input->post("draw_id");
			$data["lucky_number"] = $this->input->post("lucky_number");
		//	$data["draw_number"] = $this->get_number();	

            $this->db->select("draw_number, draw_time")->from("draws")
                ->where("draw_id !=", $data["draw_id"])
                ->order_by("draw_id", "DESC")->limit(1);
            $result = $this->db->get();
            $draw = $result->row();

            if(is_object($draw)){
                $draw_number = $draw->draw_number;
                $draw_time = $draw->draw_time;
            }	

            $data["draw_number"] = get_draw_number($draw_number, $draw_time);            	

			$draw_id = $this->Entity_Draw->run($data);

            if(!$draw_id){
                $this->load->model("Message/Message_Error");
                $errors = $this->Message_Error->get_by_entity("draws");            
                foreach($errors as $messages){
                    $message .= implode("\n", $messages);
                }

                $this->push(array(
                    "status" => 0,
                    "message" => $message
                ));
            }
            else {
                $this->push(array(
                    "status" => 1,
                    "message" => "Draw executed successfully!"
                ));
            }              

    }

    function get_number(){
        $draw_number = 0;
        $draw_time = NULL;
        $this->db->select("draw_number, draw_time")->from("draws")->order_by("draw_id", "DESC")->limit(1);
        $result = $this->db->get();
        $draw = $result->row();

        if(is_object($draw)){
            $draw_number = $draw->draw_number;
            $draw_time = $draw->draw_time;
        }        

        $now_number = get_draw_number($draw_number, $draw_time);

        $after_20_mins = get_draw_number($draw_number, time() + DRAW_INTERVAL);       

        $this->push(array(
            "status" => 1,
            "now" => $now_number,
            "after_20_mins" => $after_20_mins,
            "number_of_sec_left" => DRAW_INTERVAL
        ));

    }

    function top_winners(){
        $query = "SELECT profile_image, CONCAT(first_name, ' ', last_name) as name, SUM(IFNULL(reward, 0)) as total_reward FROM participants 
                        LEFT JOIN users ON participants.user_id = users.user_id
                        WHERE winner = 1 GROUP BY name, profile_image ORDER BY total_reward DESC LIMIT 10 ";
        $result = $this->db->query($query);
        $winners = $result->result();

        $this->push(array(
            "status" => 1,
            "winners" => $winners
        ));
    }


}
    
    
    