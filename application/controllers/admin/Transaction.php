<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends Adminaccount {

    function __construct(){
      parent::__construct($this);
      force_admin_login();
    }

    function index($parent_id=0){

        $errors = array();

        $this->db->select("DISTINCT(transaction_type)")->from("transactions");
        $transaction_types = $this->db->get()->result_array(); 

        $this->load->view("admin/layout.phtml", array(
            "view" => "transaction/index.phtml",
            "title" => "Transactions",
            "errors" => $errors,
            "parent_id" => $parent_id,
            "transaction_types" => $transaction_types
        ));

    }

    function data(){

        header("Content-Type: application/json");
        $data = $cond = array();

        $sortable = array("transaction_id", "first_name", "last_name", "email", "phone", "balance", "block");

        $status = array("Pending", "Approved", "Rejected", "-1" => "System Generated" );

        $this->db->select("*")->from("transactions")->join("users", "transactions.user_id = users.user_id")
               ->join("user_payment_methods", "transactions.user_payment_method_id = user_payment_methods.user_payment_method_id", "left");

        if(isset($_POST["user"])){           
            $cond[] = " users.first_name LIKE '%{$this->input->post('user')}%' OR users.last_name LIKE '%{$this->input->post('user')}%' OR users.email LIKE '%{$this->input->post('user')}%' ";
        }
        if(isset($_POST["amount_from"])){           
            $cond[] = " amount >= '{$this->input->post("amount_from")}'";
        }
        if(isset($_POST["amount_to"])){           
            $cond[] = " amount <= '{$this->input->post("amount_to")}'";
        }
        if(isset($_POST["transaction_type"])){           
            $cond[] = " transaction_type = '{$this->input->post("transaction_type")}'";
        }        
        if(isset($_POST["transaction_status"])){           
            $cond[] = " transaction_status = '{$this->input->post('transaction_status')}' ";
        }

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $order = ($this->input->post("draw") == 1) ? "desc" : $this->input->post("order");

        $this->db->order_by( $sortable[ (int) $this->input->post("sort") ], $order);

       // if($this->input->post("length")){
            $offset = $this->input->post("start") ? $this->input->post("start") : 0;
            $this->db->limit($this->input->post("length"), $offset );
       // }

        $result = $this->db->get();
        $transactions = $result->result_array();

        for($i=0, $count=count($transactions); $i < $count; $i++){

            $status_action = '';

            /*
            switch($transactions[$i]["transaction_status"]){

                case "0":
                    $status_action = '<a class="action" href="'. get_admin_route("transaction", "change_status", array($transactions[$i]["transaction_id"], 1)) .'" >Approve</a>';
                    $status_action .= ' | <a class="action" href="'. get_admin_route("transaction", "change_status", array($transactions[$i]["transaction_id"], 2)) .'" >Reject</a>';
                break;

                case "2":
                    $status_action = '<a class="action" href="'. get_admin_route("transaction", "change_status", array($transactions[$i]["transaction_id"], 1)) .'" >Approve</a>';
                break;

            }*/

            if($status_action) $status_action .= " | "; 

            $status_action .= ' <a  href="'. get_admin_route("transaction", "view", array($transactions[$i]["transaction_id"])) .'" >View Detail</a>';
              

            if($transactions[$i]["bank_name"]  || $transactions[$i]["account_number"] )
            $account_info = $transactions[$i]["bank_name"] . ' (' . $transactions[$i]["account_number"] . ')';
            else $account_info = "";  

            
            $transaction_time = ($transactions[$i]["transaction_time"] == '0000-00-00 00:00:00') ? '' : date("F d, Y H:i A", strtotime($transactions[$i]["transaction_time"]));
            
            $data[$i][] = (int) $transactions[$i]["transaction_id"];
            $data[$i][] = $transactions[$i]["first_name"] . ' ' . $transactions[$i]["last_name"] . " (". $transactions[$i]["email"] . ")";
            $data[$i][] = $transactions[$i]["amount"];
            $data[$i][] =  $account_info;
            $data[$i][] = $transactions[$i]["transaction_type"];
            $data[$i][] = $transaction_time;
            $data[$i][] = $status[ (int) $transactions[$i]["transaction_status"] ];
            $data[$i][] = $this->db->select("COUNT(*) as num_rows")->from("transactions")->where("parent_id", $transactions[$i]["transaction_id"])->get()->row()->num_rows;
            $data[$i][] = $status_action;
        }

        $this->db->select("COUNT(*) as num_rows")->from("transactions");        

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $num_rows = $this->db->get()->row()->num_rows;

        $json["sEcho"] = $this->input->post("sEcho");
        $json["recordsTotal"] = $num_rows;
        $json["recordsFiltered"] = $num_rows;  
        $json["data"] = $data;  
        $json["post"] = $_POST;  


        echo json_encode( $json );
        exit;
    }

    
    function view($transaction_id){

        $status = array("Pending", "Approved", "Rejected", "-1" => "System Generated");

        if($this->input->post("action") == "update_transaction"){

                $transaction_id = $this->input->post("transaction_id");
                $transaction_status = $this->input->post("transaction_status");
                $instructions = $this->input->post("instructions");

                $this->db->select("user_payment_methods.*, transactions.*")->from("transactions")->where("transaction_id", $transaction_id)
                        ->join("user_payment_methods", "transactions.user_payment_method_id = user_payment_methods.user_payment_method_id", "left");
                $transaction = $this->db->get()->row();

                $this->db->query("START TRANSACTION");
                $this->db->query("SET AUTOCOMMIT = 0");

                if( $transaction->transaction_type == "deposite" ){

                    if($transaction_status == 1){
                        $query = "UPDATE users SET balance = balance + ($transaction->amount) WHERE user_id = '{$transaction->user_id}' ";
                        $this->db->query($query);
                        /*
                        $this->db->insert("transactions", array(
                                "user_id" => $transaction->user_id,
                                "transaction_type" => $transaction->transaction_type . " (" . $status[ (int) $transaction_status ] . ")",
                                "description" => $instructions, 
                                "instructions" => $instructions, 
                                "transaction_time" => date("Y-m-d H:i:s"),
                                "parent_id" => $transaction->transaction_id
                        ));*/
                    }
                        
                    
                }
                
                else if( $transaction->transaction_type == "withdrawal" ){

                    $amount = abs($transaction->amount);

                    if($transaction_status == 2){

                        $this->db->insert("transactions", array(
                                "user_id" => $transaction->user_id,
                                "amount" => $amount,
                                "user_payment_method_id" => $transaction->user_payment_method_id,
                                "transaction_type" => $transaction->transaction_type  . " (" . $status[ (int) $transaction_status ] . ")",
                                "transaction_status" => -1,
                                "description" => "Withdrawal rejected and reversed by admin", 
                                "instructions" => $instructions, 
                                "transaction_time" => date("Y-m-d H:i:s"),
                                "parent_id" => $transaction->transaction_id,
                            ));
                        
                        $query = "UPDATE users SET balance = balance + $amount WHERE user_id = '{$transaction->user_id}' "; 
                        $this->db->query($query);
                    }
                    /*
                    else {
                        $this->db->insert("transactions", array(
                                "user_id" => $transaction->user_id,
                                "transaction_type" => $transaction->transaction_type,
                                "description" => $instructions, 
                                "instructions" => $instructions, 
                                "transaction_time" => date("Y-m-d H:i:s"),
                                "parent_id" => $transaction->transaction_id
                        ));
                    }
                    */

                    
                }    

                $update_data = array(
                        "transaction_status" => $transaction_status,
                        "instructions" => $instructions,
                    );

                if($instructions) $update_data["description"] = $instructions;   
                
                $this->db->update("transactions", $update_data, array(
                        "transaction_id" => $transaction_id
                ));

                $this->load->library("my_email", null, "my_email");

                $this->my_email->from('cointrade@trailweb.site', get_site_name());   

                $this->db->select("*")->from("users")->where("user_id", $transaction->user_id);
                $user = $this->db->get()->row();

//                echo '<pre>'; print_r($user); print_r($transaction); exit;

                $this->db->select("user_payment_methods.*, transactions.*")->from("transactions")->where("transaction_id", $transaction_id)
                        ->join("user_payment_methods", "transactions.user_payment_method_id = user_payment_methods.user_payment_method_id", "left");
                $transaction = $this->db->get()->row();

                $this->my_email->to($user->email);   

                $this->my_email->subject("Status of one of your transaction at " . get_site_name() . " is changed");

                $status = array("Pending", "Approved", "Rejected/Unapproved", "-1" => "System Generated") ;

                $message = "Status of one of your transaction at " . get_site_name() . " is changed, details are mentioned below.";

                $message = '<table>';
                $message = '<tr><td>Amount</td><td>'.$transaction->amount.'</td></tr>';
                $message = '<tr><td>Account</td><td>'.$transaction->bank_name.', '.$transaction->account_number.'</td></tr>';
                $message = '<tr><td>Instructions</td><td>'.$transaction->instructions.'</td></tr>';
                $message = '<tr><td>Description</td><td>'.$transaction->description.'</td></tr>';
                $message = '<tr><td>Type</td><td>'.$transaction->transaction_type.'</td></tr>';
                $message = '<tr><td>Status</td><td>'.$status[ (int) $transaction->transaction_status ].'</td></tr>';
                $message = '<tr><td>Trasaction Create Time</td><td>'. date( "F d, Y h:i A ", strtotime( $transaction->transaction_time ) ) .'</td></tr>';
                $message = '</table>';

                $this->my_email->message($message);

                $this->my_email->send();


                $this->db->query("COMMIT");
                
                redirect_to("admin/transaction");

        }

        $errors = array();

        $this->db->select("*")->from("transactions")->where("transaction_id", $transaction_id)
                ->join("users", "transactions.user_id = users.user_id")
                ->join("user_payment_methods", "user_payment_methods.user_payment_method_id = transactions.user_payment_method_id", "left")
                ;
        $transaction = $this->db->get()->row();

        $this->load->view("admin/layout.phtml", array(
            "view" => "transaction/view.phtml",
            "title" => "Transaction Detail",
            "errors" => $errors,            
            "transaction" => $transaction
        ));     

    }


    function deposite(){

        $this->load->model("Entity/Entity_User");
        $this->load->model("Message/Message_Error");
        
        $success = 0;

        if($this->input->post("action") == "deposite"){
        
        $amount = $this->input->post("amount");
        $user_payment_method_id = $this->input->post("user_payment_method_id");
        $instructions = ucfirst($this->input->post("instructions"));
        $user_id = $this->input->post("user_id");

        if(!$instructions){
            $this->Message_Error->trigger("deposite", "instructions", "Please enter instructions");
        }

        if( (int) $amount <= 0 ){
            
            $this->Message_Error->trigger("deposite", "amount", "Amount should be number and greater than zero");
        }

        $this->db->select("*")->from("users")->where("user_id", $user_id);
        $user = $this->db->get()->row();

        if(!is_object($user)){
            $this->Message_Error->trigger("deposite", "user_id", "Please select valid user");
        }        

        /*
        $this->db->select("user_id")->from("user_payment_methods")->where(array(
            "user_id" => 0,
            "user_payment_method_id" => $user_payment_method_id
        ));

        $result = $this->db->get();

        $admin_payment_method = $result->row();

        if(!is_object($admin_payment_method)){
            
            $this->Message_Error->trigger("deposite", "user_payment_method_id", "Desposite method is invalid");

        } 

        */

        $errors = $this->Message_Error->get_by_entity("deposite");

        if(count($errors) == 0){
        
        $this->db->insert("transactions", array(
               // "sender_object" => "users",
                "user_id" => $user_id,
                "amount" => $amount,
                //"user_payment_method_id" => $user_payment_method_id,
                "transaction_type" => "deposite",
                "transaction_status" => 1,
                "instructions" => ucfirst($instructions), 
                "transaction_time" => date("Y-m-d H:i:s")
            ));
         
          $transaction_id = $this->db->insert_id(); 
           
         if($transaction_id){
             $query = "UPDATE users SET balance = balance + $amount WHERE user_id = '{$user_id}' ";
             $this->db->query($query);
             $success = 1;
         }          
         else {
             //$this->Message_Error->trigger("deposite", "user_payment_method_id", "Desposite method is invalid");  
         }  

       }  

       }

       $errors = $this->Message_Error->get_by_entity("deposite");

       $this->db->select("*")->from("users");
       $users = $this->db->get()->result();

       $this->db->select("*")->from("user_payment_methods")->where(array(
            "user_id" => 0
        ));

        $deposite_methods = $this->db->get()->result();

       $this->load->view("admin/layout.phtml", array(
            "view" => "transaction/deposite.phtml",
            "title" => "Deposite",
            "errors" => $errors,            
            "users" => $users,
            "deposite_methods" => $deposite_methods,
            "success" => $success
        ));   
    }



    function withdraw(){

        $this->load->model("Entity/Entity_User");
        $this->load->model("Message/Message_Error");
        
        $success = 0;

        if($this->input->post("action") == "withdraw"){
        
        $amount = $this->input->post("amount");
        $user_payment_method_id = $this->input->post("user_payment_method_id");
        $instructions = ucfirst($this->input->post("instructions"));
        $user_id = $this->input->post("user_id");

        if(!$instructions){
            $this->Message_Error->trigger("withdraw", "instructions", "Please enter instructions");
        }

        if( (int) $amount <= 0 ){            
            $this->Message_Error->trigger("withdraw", "amount", "Amount should be number and greater than zero");
        }

        $this->db->select("balance")->from("users")->where("user_id", $user_id);
        $user = $this->db->get()->row();

        if(!is_object($user)){
            $this->Message_Error->trigger("withdraw", "user_id", "Please select valid user");

            
        }
        else if($user->balance < $amount){
                $this->Message_Error->trigger("withdraw", "balance", "User does not have enough balance");
        }

        /*
        $this->db->select("user_id")->from("user_payment_methods")->where(array(
            "user_id" => $user_id,
            "user_payment_method_id" => $user_payment_method_id
        ));

        $result = $this->db->get();

        $admin_payment_method = $result->row();

        if(!is_object($admin_payment_method)){
            
            $this->Message_Error->trigger("withdraw", "user_payment_method_id", "Widthdraw method is invalid");

        } */

        $errors = $this->Message_Error->get_by_entity("withdraw");

        if(count($errors) == 0){

        $amount *= (-1);    
        
        $this->db->insert("transactions", array(
               // "sender_object" => "users",
                "user_id" => $user_id,
                "amount" => $amount,
              //  "user_payment_method_id" => $user_payment_method_id,
                "transaction_type" => "withdraw",
                "transaction_status" => 1,
                "instructions" => ucfirst($instructions), 
                "transaction_time" => date("Y-m-d H:i:s")
            ));
         
          $transaction_id = $this->db->insert_id(); 
           
         if($transaction_id){
             $query = "UPDATE users SET balance = balance + ($amount) WHERE user_id = '{$user_id}' ";
             $this->db->query($query);
             $success = 1;
         }          
         else {
            
         }  

       }  

       }

       $errors = $this->Message_Error->get_by_entity("withdraw");

       $this->db->select("*")->from("users");
       $users = $this->db->get()->result();

       

       $this->load->view("admin/layout.phtml", array(
            "view" => "transaction/withdraw.phtml",
            "title" => "Withdraw",
            "errors" => $errors,            
            "users" => $users,
            //"deposite_methods" => $deposite_methods,
            "success" => $success
        ));   
    }


    function ajax_get_user_payment_method(){
        $user_id = $this->input->post("user_id"); 
        $this->db->select("*")->from("user_payment_methods")->where("user_id", $user_id);
        $payment_methods = $this->db->get()->result();
        $list = '';
        for($i=0, $count = count($payment_methods); $i < $count; $i++){
                $account_type =  $payment_methods[$i]->method_name == "bank" ? "Account Number " : " Mobile Number ";
                $list .= '<div><input type="radio" name="user_payment_method_id" value="'. $payment_methods[$i]->user_payment_method_id . '"> 
                <strong>' .  ucwords($payment_methods[$i]->bank_name)  . '</strong>,
                 <label> ( '. $account_type . $payment_methods[$i]->account_number . ') </label>, ';
                if($payment_methods[$i]->method_name == "bank"){
                    $list .= "Branch Code " . $payment_methods[$i]->branch_code;
                } 
                else if($payment_methods[$i]->method_name == "mobile"){
                    $list .= "NIC # " . $payment_methods[$i]->cnic;
                }    

            $list .= '</div>';
        }

        echo $list; 
        exit;
        
    }




}    