<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends Adminaccount {

    function __construct(){
      parent::__construct($this);
      force_admin_login();
    }

    function index($parent_id=0){

        $errors = array();        

        $this->load->view("admin/layout.phtml", array(
            "view" => "payment/index.phtml",
            "title" => "Payment Methods",
            "errors" => $errors                        
        ));

    }

    function data(){

        header("Content-Type: application/json");
        $data = $cond = array();

        $sortable = array("user_payment_method_id", "method_name", "bank_name", "account_number", "account_title", "branch_code", "cnic");
        

        $this->db->select("*")->from("user_payment_methods")->where("user_id", 0);

        if(isset($_POST["method_name"])){           
            $cond[] = " method_name LIKE '%{$this->input->post('method_name')}%' ";
        }
        if(isset($_POST["bank_name"])){           
            $cond[] = " bank_name LIKE '%{$this->input->post("bank_name")}%'";
        }
        if(isset($_POST["account_number"])){           
            $cond[] = " account_number LIKE '%{$this->input->post("account_number")}%'";
        }
        if(isset($_POST["account_title"])){           
            $cond[] = " account_title LIKE '%{$this->input->post("account_title")}%'";
        }        
        if(isset($_POST["branch_code"])){           
            $cond[] = " branch_code LIKE '%{$this->input->post('branch_code')}%' ";
        }

        if(isset($_POST["cnic"])){           
            $cond[] = " cnic LIKE '%{$this->input->post('cnic')}%' ";
        }

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }


        $order = ($this->input->post("draw") == 1) ? "desc" : $this->input->post("order");
        $this->db->order_by( $sortable[ (int) $this->input->post("sort") ], $order);

        if($this->input->post("length")){
            $offset = $this->input->post("start") ? $this->input->post("start") : 0;
            $this->db->limit($this->input->post("length"), $offset );
        }

        $result = $this->db->get();
        $payment_methods = $result->result_array();

        for($i=0, $count=count($payment_methods); $i < $count; $i++){
            
            $data[$i][] = $payment_methods[$i]["user_payment_method_id"];
            $data[$i][] = $payment_methods[$i]["method_name"];
            $data[$i][] = $payment_methods[$i]["bank_name"] ;
            $data[$i][] = $payment_methods[$i]["account_number"];
            $data[$i][] = $payment_methods[$i]["account_title"];
            $data[$i][] = $payment_methods[$i]["branch_code"];
            $data[$i][] = $payment_methods[$i]["cnic"];
            $data[$i][] = '<a href="' . get_admin_route("payment", "save", array($payment_methods[$i]['user_payment_method_id'])) . '">Edit</a> | <a href="'
                            . get_admin_route("payment", "delete", array($payment_methods[$i]['user_payment_method_id'])) . '">Delete</a>';
        }

        $json["sEcho"] = $this->input->post("sEcho");
        $json["recordsTotal"] = $result->num_rows();
        $json["recordsFiltered"] = $result->num_rows();  
        $json["data"] = $data;  
        $json["post"] = $_POST;  


        echo json_encode( $json );
        exit;
    }

    function save($user_payment_method_id = null){

       $this->load->model("Entity/Entity_UserPaymentMethod");      

      $errors = array();  

      if($this->input->post("action") == "save_payment_method") {          

           
			
            $this->load->model("Message/Message_Error");
			
            $data["method_name"] = $this->input->post("method_name");
            $data["account_number"] = $this->input->post("account_number");
            $data["user_id"] = 0;
            $data["bank_name"] = $this->input->post("bank_name");

            if($data["method_name"] == "bank"){
                
                $data["account_title"] = $this->input->post("account_title");
                
                $data["bank_address"] = $this->input->post("bank_address");
                $data["branch_code"] = $this->input->post("branch_code");
            }    
            else if($data["method_name"] == "mobile"){			
                $data["cnic"] = $this->input->post("cnic");
            }     
            else {
                $this->Message_Error->trigger("user_payment_methods", "method_name", "Method name should be either 'Mobile' or 'Bank'");
            }

            if($user_payment_method_id){

                $data["user_payment_method_id"] =  $user_payment_method_id;  

                          

                $this->Entity_UserPaymentMethod->modify($data);

                //echo '<pre>'; print_r($data); echo '</pre>'; exit;   
            }	
            else {
                $user_payment_method_id = $this->Entity_UserPaymentMethod->add($data);
            }
       
            $errors = $this->Message_Error->get_by_entity("user_payment_methods");

            if(count($errors) == 0){
                
                redirect_to("admin", "payment");
            }

        }   

        $current_paymnet_method = $this->db->select("*")->from("user_payment_methods")
        ->where("user_payment_method_id", $user_payment_method_id)->get()->row_array();

       // echo '<pre>'; print_r($current_paymnet_method); echo '</pre>'; exit;

        $this->load->view("admin/layout.phtml", array(
            "view" => "payment/save.phtml",
            "title" => "Payment Method Save",
            "errors" => $errors,
            "current_paymnet_method" => $current_paymnet_method
        )); 

      
        
    }

    function delete($user_payment_method_id){
         $this->load->model("Entity/Entity_UserPaymentMethod");    
         $this->Entity_UserPaymentMethod->remove($user_payment_method_id);

         redirect_to("admin", "payment");
    }


}    