<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Draw extends Adminaccount {

    function __construct(){
      parent::__construct($this);
      force_admin_login();
    }

    function index(){

        $errors = array();

        /*$this->db->select("DISTINCT(transaction_type)")->from("transactions");
        $transaction_types = $this->db->get()->result_array(); 
*/
        $this->load->view("admin/layout.phtml", array(
            "view" => "draw/index.phtml",
            "title" => "Draws",
            "errors" => $errors,
         //   "parent_id" => $parent_id,
         //   "transaction_types" => $transaction_types
        ));

    }

    function data(){

        header("Content-Type: application/json");
        $data = $cond = array();

        $sortable = array("draw_time", "lucky_number", "draw_number");

        $status = array("Pending", "Approved", "Rejected");

        $this->db->select("*")->from("draws");

        if(isset($_POST["lucky_number"])){           
            $cond[] = "lucky_number = '{$this->input->post('lucky_number')}'";
        }

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $order = ($this->input->post("draw") == 1) ? "desc" : $this->input->post("order");

        $this->db->order_by( $sortable[ (int) $this->input->post("sort") ], $order);

        $this->db->limit((int) $this->input->post("length"), (int) $this->input->post("start"));

        $result = $this->db->get();
        $draws = $result->result_array();

        for($i=0, $count=count($draws); $i < $count; $i++){

            $status_action = '';
            
            $data[$i][] = date("F d, Y h:i A", strtotime($draws[$i]["draw_time"]));
            $data[$i][] = $draws[$i]["lucky_number"];
            $data[$i][] = $draws[$i]["draw_number"] . $draws[$i]["lucky_number"];            
            $data[$i][] = date("F d, Y h:i A", strtotime($draws[$i]["creation_date"])) ;
            $data[$i][] = $this->db->select("COUNT(*) as num_rows")->from("participants")->where("draw_id", $draws[$i]["draw_id"])->get()->row()->num_rows;
            $data[$i][] = $this->db->select("COUNT(*) as winners")->from("participants")
                        ->where(array("draw_id" => $draws[$i]["draw_id"], "winner" => 1))->get()->row()->winners;
            
        }

        $this->db->select("COUNT(*) as num_rows")->from("draws");        

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $num_rows = $this->db->get()->row()->num_rows;

        $json["sEcho"] = $this->input->post("sEcho");
        $json["recordsTotal"] = $num_rows;
        $json["recordsFiltered"] = $num_rows;  
        $json["data"] = $data;  
        $json["post"] = $_POST;  


        echo json_encode( $json );
        exit;
    }

    function lucky_number_statistics(){

        $draw_id = 0;
        $lucky_number = NULL;
        $investors = array();

        $this->db->select("*")->from("draws")->order_by("draw_id", "desc")->limit(1);
        $draw = $this->db->get()->row();
        $draw_time = $draw->draw_time;
        $lucky_number = $draw->lucky_number; 
        $draw_id = $draw->draw_id; 
        if(!empty($draw->draw_number) && $draw->draw_number != 0){
            $temp = strtotime($draw_time) + DRAW_INTERVAL;
            $draw_time = date("Y-m-d H:i:s", $temp);
            $lucky_number = null; 
            $draw_id = null;
            
       }

       

       $this->load->model("Entity/Entity_Draw");

       $lucky_numbers = $this->Entity_Draw->get_luck_number_for_draw($draw_time);

        if( !( $lucky_number === 0 || ($lucky_number >= 1 && $lucky_number <= 9  ) ) ){            

            /*

            foreach($lucky_numbers as $number => $coins){
                $lucky_number = $number;
                break;
            }
            */

            $lucky_number = $this->Entity_Draw->get_random_lucky_number($lucky_numbers);

        }

        $total_sale = 0;

        $investers = 0;

        foreach($lucky_numbers as $number => $coins){

            $total_sale += (int) $coins;

            $lucky_numbers[$number] = (int) $coins; 

            $queries[] = $query = "SELECT lucky_number, COUNT(*) as invest FROM participants WHERE draw_time = '{$draw_time}' AND lucky_number = '{$number}' 
            GROUP BY lucky_number";

            $result = $this->db->query($query); 
            
            $investment = $result->row();

            if(is_object($investment)){
                $investers = $investment->invest;  
            }

            $investors[$number] = (int) $investers;
            
        }

        ksort($lucky_numbers);

         $this->load->view("admin/layout.phtml", array(
            "view" => "draw/lucky_number_statistics.phtml",
            "title" => "Draws",
            "draw_id" => $draw_id,
            "php_draw_time" => strtotime($draw_time),           
            "lucky_numbers" => $lucky_numbers,
            "lucky_number" => $lucky_number,           
            "total_sale" => $total_sale,         
            "draw" => $draw,
            "investors" => $investors,
            "queries" => $queries, 
            "result" => $result 
        ));

    }

    function set_lucky_number($draw_id, $lucky_number, $php_draw_time){

        $this->db->select("*")->from("draws")->where("draw_id", $draw_id);
        $draw = $this->db->get()->row();

        if(is_object($draw)){
            /*
            $this->db->where("draw_id", $draw_id);
            $this->db->update("draws", array("lucky_number" => $lucky_number));
            */
            $query = "UPDATE draws SET lucky_number = '{$lucky_number}', admin_override = 1 WHERE draw_id = '{$draw_id}'";
            $this->db->query($query);
            $this->db->query("COMMIT");
          //  echo $query;             exit;
        }
        else{
            $this->load->model("Entity/Entity_Draw");
            $this->Entity_Draw->add(array(
                "draw_time" => date("Y-m-d H:i:s", $php_draw_time),
                "lucky_number" => $lucky_number,
                "admin_override" => 1 
            ));
        }

        redirect_to("admin/draw", "lucky_number_statistics");
    }


}    