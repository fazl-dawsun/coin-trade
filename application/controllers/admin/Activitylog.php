<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require_once(dirname(__DIR__) . "/Adminaccount.php");

class Article extends Adminaccount {

    function __construct(){
        parent::__construct($this);
    }

    function index(){
      $errors = array();
      $this->load->view("admin/layout.phtml", array(
          "view" => "article/index.phtml",
          "title" => "Article",
          "errors" => $errors
      ));
      }

    function add(){

          $errors = array();
          $this->load->model("Entity/Entity_Article");
          $this->load->model("Message/Message_Error");
          if($this->input->post("action") == "add_article"){

            $data["article_title"] = $this->input->post('article_title');
            $data["article_id"] = (int) $this->input->post('article_id');
            $data["article_description"] = $this->input->post('article_description');

            $article_id = $this->Entity_Article->add($data);
              if( !$article_id  ){
                  $errors = $this->Message_Error->get_by_entity("article");
              }
              else {
                  redirect_to("admin/article");
              }

          }

          $this->load->view("admin/layout.phtml", array(
              "view" => "article/save.phtml",
              "title" => "Add Article",
              "errors" => $errors,
              "action" => "add_article"
          ));
      }


    function edit($article_id ){

      $errors = array();
      $article = array();

      $this->load->model("Entity/Entity_Article");
      $this->load->model("Message/Message_Error");

      if($this->input->post("action") == "edit_article"){

          $data["article_id"] = $article_id;
          $data["article_title"] = $this->input->post('article_title');
          $data["article_description"] = $this->input->post('article_description');

          $article_id = $this->Entity_Article->modify($data);
          if( !$article_id  ){
              $errors = $this->Message_Error->get_by_entity("article");
          }
          else {
              redirect_to("admin/article");
          }

      }

      $this->db->select("article_id, article_title , article_description , article_thumbnail ,article_image  ")
                ->from("articles")->where('article_id', $article_id);

      $article = $this->db->get()->row_array();

      $this->load->view("admin/layout.phtml", array(
          "view" => "article/save.phtml",
          "title" => "Edit Article",
          "errors" => $errors,
          "article"  => $article,
          "action" => "edit_article"
      ));

    }

    function data(){
        header("Content-Type: application/json");
        $this->db->select("article_id , article_title , article_description ")->from("articles");
        $resultdata = $this->db->get()->result_array();

        for($i=0, $count=count($resultdata); $i < $count; $i++){
            $data[$i] = array_values($resultdata[$i]);
           // $data[$i][] = '<a href="'.get_route("admin/article", "add", array()).'">Add</a>';
            $data[$i][] = '<a href="'.get_route("admin/article", "edit", array($resultdata[$i]['article_id'])).'">Edit</a>';
            $data[$i][] = '<a href="'.get_route("admin/article", "remove", array($resultdata[$i]['article_id'])).'">Delete</a>';

        }
        echo json_encode(array("data" => $data) );
        exit;
      }
}
