<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Adminaccount {

    function __construct(){
      parent::__construct($this);
      force_admin_login();
    }

    function index(){

        $errors = array();

        $this->load->view("admin/layout.phtml", array(
            "view" => "user/index.phtml",
            "title" => "Users",
            "errors" => $errors
        ));

    }

    function add(){

        $this->load->model("Data/Data_User");
        $this->Data_User->first_name = $this->input->post("first_name");
        $this->Data_User->last_name = $this->input->post("last_name");
        $this->Data_User->email = $this->input->post("email");
        $user_id = $this->Data_User->insert();
        $this->db->select("*")->from("users");
        $users = $this->db->get()->result();

        header("Content-Type: application/json");
        echo json_encode($users);
        exit;

    }


    function get_data($user_id){

      // header("Content-Type: application/json");
      $errors =  array();
      $this->db->select("user_id ,first_name, last_name, profile_slug, email, phone, dob, city_id, user_name, password, annual_income, balance, description")->from("users")->where('user_id' , $user_id);
      $user = $this->db->get()->row_array();

      $this->db->select("city_id ,city_name")->from("cities");
      $city = $this->db->get()->result_array();

      $this->load->view("admin/layout.phtml", array(
          "view" => "user/save.phtml",
          "title" => "Content Management System",
          "errors" => $errors,
          "data" =>  $user,
          "city" => $city
      ));
    }

    function save($user_id){

      $errors = array();
      $this->load->model("Entity/Entity_User");
      $this->load->model("Message/Message_Error");

      if($this->input->post("action") == "save_user"){

          $data["user_id"] = $user_id;
          
          $data["first_name"] = $this->input->post('first_name');
          $data["last_name"] = $this->input->post('last_name');
          
          $data["email"] = $this->input->post('email');
          $data["phone"] = $this->input->post('phone');
          $data["date_of_birth"] = $this->input->post('date_of_birth');
          $data["state"] = $this->input->post('state');
          $data["gender"] = $this->input->post('gender');
          $data["country"] = $this->input->post('country');
          $data["city"] = $this->input->post('city');
          $data["balance"] = $this->input->post('balance');

          if(is_file($_FILES["profile_image"]["tmp_name"])){
                $content = file_get_contents($_FILES["profile_image"]["tmp_name"]);
                $data["profile_image"] = 'data:' . $_FILES["profile_image"]["type"] . ';base64,' . base64_encode($content);
          }

          $this->db->set("block", $this->input->post('block'));

          $this->Entity_User->save_profile($data);

          $errors = $this->Message_Error->get_by_entity("users");

          if( count($errors) > 0  ){
              
          }
          else {
              redirect_to("admin/user");
          }

      }

      $this->load->view("admin/layout.phtml", array(
          "view" => "user/save.phtml",
          "title" => "Save User",
          "errors" => $errors,
          "data" => $this->db->select("*")->from("users")->where("user_id", $user_id)->get()->row_array()
      ));

    }

    function edit($user_id = '1'){

      $errors = array();
      $this->load->model("Entity/Entity_User");
      $this->load->model("Message/Message_Error");
      if($this->input->post("action") == "edit_page"){

          $data["user_id"] = $user_id;
          $data["parent_id"] = $this->input->post('parent_id');
          $data["first_name"] = $this->input->post('first_name');
          $data["last_name"] = $this->input->post('last_name');
          $data["profile_slug"] = $this->input->post('profile_slug');
          $data["email"] = $this->input->post('email');
          $data["phone"] = $this->input->post('phone');
          $data["dob"] = $this->input->post('dob');
          $data["city_id"] = $this->input->post('city_id');
          $data["user_name"] = $this->input->post('user_name');
          $data["password"] = $this->input->post('password');
          $data["annual_income"] = $this->input->post('annual_income');
          $data["balance"] = $this->input->post('balance');
          $user_id = $this->Entity_User->modify($data);
          if( !$user_id  ){
              $errors = $this->Message_Error->get_by_entity("user");
          }
          else {
              redirect_to("admin/user");
          }

      }

      $this->load->view("admin/layout.phtml", array(
          "view" => "user/index.phtml",
          "title" => "Content Management System",
          "errors" => $errors
      ));

    }
    function data(){
        header("Content-Type: application/json");
        $data = $cond = array();

        $sortable = array("user_id", "first_name", "last_name", "email", "phone", "balance", "block");

        $status = array("Active", "Block");

        $this->db->select("user_id, first_name, last_name, email, phone, balance, block")->from("users");

        if(isset($_POST["first_name"])){           
            $cond[] = " first_name LIKE '%{$this->input->post("first_name")}%'";
        }
        if(isset($_POST["last_name"])){           
            $cond[] = " last_name LIKE '%{$this->input->post("last_name")}%'";
        }
        if(isset($_POST["email"])){           
            $cond[] = " email LIKE '%{$this->input->post("email")}%'";
        }
        if(isset($_POST["phone"])){           
            $cond[] = " phone LIKE '%{$this->input->post("phone")}%'";
        }
        if(isset($_POST["status"])){           
            $cond[] = " block = {$_POST['status']} ";
        }

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $order = ($this->input->post("draw") == 1) ? "desc" : $this->input->post("order");

        $this->db->order_by( $sortable[ (int) $this->input->post("sort") ], $order);

        $result = $this->db->get();
        $users = $result->result_array();

        for($i=0, $count=count($users); $i < $count; $i++){
            /*$user_id = $users[$i]["user_id"];
            unset($users[$i]["user_id"]);
            $data[$i] = array_values($users[$i]);
            */

            $status_action = ($users[$i]["block"]) ? '<a class="action" href="'. get_admin_route("user", "block", array($users[$i]["user_id"], 0)) .'" >Activate</a>' 
            : '<a class="action" href="'. get_admin_route("user", "block", array($users[$i]["user_id"], 1)) .'" >Block</a>';

            $data[$i][] = $users[$i]["user_id"];
            $data[$i][] = $users[$i]["first_name"];
            $data[$i][] = $users[$i]["last_name"];
            $data[$i][] = $users[$i]["email"];
            $data[$i][] = $users[$i]["phone"];
            $data[$i][] = $users[$i]["balance"];
            $data[$i][] = $status[ (int) $users[$i]["block"] ];
            $data[$i][] = '<a href="'. get_admin_route("user", "save", array($users[$i]["user_id"])) .'" >Edit</a> | ' . $status_action;
        }

        $this->db->select("COUNT(*) as num_rows")->from("users");        

        if(count($cond) > 0 ){
           $this->db->where(implode(" AND ", $cond)) ; 
        }

        $num_rows = $this->db->get()->row()->num_rows;

        $json["sEcho"] = $this->input->post("sEcho");
        $json["recordsTotal"] = $num_rows;
        $json["recordsFiltered"] = $num_rows;  
        $json["data"] = $data;  
        $json["post"] = $_POST;  
        $json["cond"] = $cond;  
        $json["cond"] = $cond;  


        echo json_encode( $json );
        exit;
    }

    function remove($id){
        $errors = array();

        $this->load->model("Entity/Entity_User");
        $this->load->model("Message/Message_Error");

        $this->Entity_User->remove($id);
        redirect_to("admin/user");
    }

    function block($user_id, $status = 1){

      //  echo "user_id: " . $user_id . ", <br> status: " . $status; exit;
        /*
        $this->db->where("user_id", $user_id);
        $this->db->update("users", array("block"=>$status));
        */
        $query = "UPDATE users SET block = '{$status}' WHERE user_id = '{$user_id}'";
        $this->db->query($query);
       // echo $query; exit;
        
        redirect_to("admin/user");
    }

    function profile_image($user_id){
        $this->db->select("profile_image")->from("users")->where("user_id", $user_id);
        $user = $this->db->get()->row();

        $profile_image = $user->profile_image;

     //   echo $profile_image; exit;
      //  $profile_image = str_replace('data:image/jpeg;base64,', '', $profile_image);

        $tmp = explode(",", $profile_image);
        $code_binary = base64_decode( $tmp[1] );
        
        $image= imagecreatefromstring($code_binary);
        
        header('Content-Type: image/jpg');
        imagejpeg($image);
        imagedestroy($image);
        exit;
        
    }

}
