<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require_once __DIR__ . "/Root.php";

class Adminaccount extends Root{

    function __construct($invoker = NULL){

        parent::__construct((is_object($invoker)) ? $invoker : $this);

        $controller = $this->uri->segment(1);
        $action = $this->uri->segment(2);
        
        if(!is_admin_user()){
            if(!($controller == "adminaccount" && in_array( $action,  array("login", "logout") ))){
                redirect_to("adminaccount", "login");
            }
        }
        

    }

    function index() {

        $this->db->select("COUNT(*) as total_users")->from("users");
        $total_users = $this->db->get()->row()->total_users;

        $this->db->select("COUNT(*) as total_draws")->from("draws");
        $total_draws = $this->db->get()->row()->total_draws;

        $this->db->select("COUNT(*) as total_participants")->from("participants");
        $participants = $this->db->get()->row()->total_participants;

        $this->db->select("SUM(IFNULL(coins, 0)) as total_sale")->from("participants");
        $total_sale = $this->db->get()->row()->total_sale;

        $this->db->select("COUNT(*) as winners")->from("participants")->where("winner", 1);
        $winners = $this->db->get()->row()->winners;

        $this->load->view('admin/layout.phtml', array(
            "view" => "dashboard.phtml",
            "users" => $total_users,
            "draws" => $total_draws,
            "coils" => $total_sale,
            "participants" => $participants,
            "winners" => $winners
        ));
      

    }

    function logout(){
        $this->load->model("Entity/Entity_Admin");
        $this->Entity_Admin->logout();
        redirect_to("adminaccount", "login");
    }

    function login(){
        $errors = array();
        $this->load->model("Entity/Entity_Admin");

       if(is_admin_user()){
           redirect_to("adminaccount", "index");
       }

        if($this->input->post('action') == "login"){

            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if( !$this->Entity_Admin->login($email, $password) ){
                $errors[] = "Login error, your user name/email or password is incorrect ";
            }
            else {

               redirect_to("adminaccount");

            }

             

        }

        $this->load->view("admin/layout.phtml", array(
            "view" => "login.phtml",
            "title" => "Login",
            "errors" => $errors
        ));
    }


    function change_password(){
          $this->load->model("Entity/Entity_Admin");
          $this->Entity_Admin->force_login();

          $errors = array();
          $success = 0;

          $confirm_new_password = $new_password = $current_password = "";

          if($this->input->post("action") == "change_password"){

             // echo '<pre>'; print_r($_POST); exit;

              $current_password = $this->input->post("current_password");
              $new_password = $this->input->post("new_password");
              $confirm_new_password = $this->input->post("confirm_new_password");

              $success = $this->Entity_Admin->change_password( $current_password, $new_password, $confirm_new_password);
              $this->load->model("Message/Message_Error");
              $errors = $this->Message_Error->get_by_entity("admins");

              if(count($errors) == 0 ) $success = 1;

          }



          $this->load->view("admin/layout.phtml", array(
              "view" => "change-password.phtml",
              "title" => "Change Password",
              "errors" => $errors,
              "success" => $success,
              "current_password" => $current_password,
              "new_password" => $new_password,
              "confirm_new_password" => $confirm_new_password
          ));
      }

}