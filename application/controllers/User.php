<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Front{

    protected $data = array();
    protected $user_id; 

    function __construct($invoker = NULL){
        parent::__construct((is_object($invoker)) ? $invoker : $this);        

    }

    function index(){

        $user_info = get_credentials();
        auth_access_only();
        $this->db->select("email, first_name, last_name, profile_image, gender, date_of_birth, city, state, country, phone, balance")
        ->from("users")->where("user_id", $user_info["user_id"]);
        $result = $this->db->get();
        $user = $result->row();

        $this->push(array(
            "status" => 1,
            "profile" => $user            
        )); 
    }


    function get_profile(){
        auth_access_only();
        $user_info = get_credentials();
        $this->db->select("email, first_name, last_name, profile_image, gender, date_of_birth, city, state,user_fbid, country, phone, balance")
                ->from("users")->where("user_id", $user_info["user_id"]);
        $result = $this->db->get();
        $user = $result->row();

        $this->push(array(
            "status" => 1,
            "profile" => $user            
        )); 
    }

    function get_balance(){
        auth_access_only();
        $user_info = get_credentials();
        $this->db->select("balance")->from("users")->where("user_id", $user_info["user_id"]);
        $result = $this->db->get();
        $user = $result->row();

        $this->push(array(
            "status" => 1,
            "balance" => $user->balance            
        )); 
    }  
    
    function upload_image($user_id){
            $target_path = "uploads/";
            $target_path = $target_path . basename( $_FILES['file']['name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            //$this->db->set('profile_image',$target_path );
                $fullpath = base_url().''.$target_path;
            $data = array('profile_image' => $fullpath);
            $this->db->where('user_id', $user_id);
            $this->db->update('users', $data); 
            }
        
        }
    function get_info(){
        auth_access_only();
        $this->db->select("balance")->from("users")->where("user_id", $this->user_id);
        $result = $this->db->get();
        $user = $result->row();

        $this->load->model("Entity/Entity_User");
        $this->load->model("Entity/Entity_Draw");        

        $profile_images = array();
        $draw_number = 0;
        $pre_lucky_number = $pre_draw_number = $draw_time = NULL;
        $this->db->select("draw_id, lucky_number, draw_number, draw_time")->from("draws")->order_by("draw_id", "DESC")->limit(1);
        $result = $this->db->get();
        $draw = $result->row();

        $this->Entity_Draw->run(array(
             "draw_id" => (int) $draw->draw_id,
             "pre_run" => PRE_DRAW_INTERVAL   
        ));

        $draw_number = $draw->draw_number;
        $draw_time = $draw->draw_time;
        $lucky_number = $draw->lucky_number;

        $last_draw_id = $last_draw_number = $last_draw_lucky_number = NULL;

        $next_draw_time = strtotime($draw_time);

        $previous_draw_query = "SELECT draw_id, lucky_number, draw_number, draw_time FROM draws WHERE draw_time < '" . date("Y-m-d H:i:s") 
                        . "' ORDER BY draw_id DESC LIMIT 1";

        $result = $this->db->query($previous_draw_query);

        $previous_draw = $result->row();



        $now_number = $previous_draw->draw_number + SYSTEM_NUMBER;// get_draw_number($previous_draw->draw_number, $next_draw_time);         

        if( $draw->draw_number !=0 && !$draw->draw_number ){
             $next_draw_time += PRE_DRAW_INTERVAL; 
             
        }     
        else {
            //$this->db->select("lucky_number, draw_number, draw_time")->from("draws")->where()->order_by("draw_id", "DESC")->limit(1);
            $query = "SELECT draw_id, lucky_number, draw_number, draw_time FROM draws WHERE draw_number IS NOT NULL ORDER BY draw_id DESC LIMIT 1";
            $last_draw = $this->db->query($query)->row();
            $draw_number = $last_draw->draw_number;
            $last_draw_number = $last_draw->draw_number;
            $pre_lucky_number = $last_draw_lucky_number = $last_draw->lucky_number;            
            $last_draw_id = $last_draw->draw_id;
        }            

        $diff = $next_draw_time - time() ;        

        $after_9_mins = get_draw_number($draw_number, $next_draw_time);

        $user_info = get_credentials();

        $now = date("Y-m-d H:i:s");

        $query = "SELECT user_id, SUM(coins) as user_investment FROM participants                          
                        WHERE draw_time = '" . date("Y-m-d H:i:s", $next_draw_time) . "' 
                        GROUP BY user_id ORDER BY user_investment DESC";
        $entries = $this->db->query($query)->result();        

        if( !( $lucky_number === 0 || ($lucky_number >= 1 && $lucky_number <= 9) )){
            
            $lucky_numbers = $this->Entity_Draw->get_luck_number_for_draw($draw_time);
/*
            foreach($lucky_numbers as $number){
                $lucky_number = (int) $number;
                break;
            }
*/
            $lucky_number = (int) $this->Entity_Draw->get_random_lucky_number($lucky_numbers);    
        }

     //   $lucky_number = 

        $cond = array(
            "user_id" => $this->user_id,
            "draw_id" => $last_draw_id,
            "winner" => 1
        );

        $this->db->select("lucky_number, draw_time, reward")->from("participants")->where($cond);      

        $my_earnings = $this->db->get()->result();

        $this->db->select("SUM( IFNULL(reward, 0) ) as total_earning")->from("participants")->where($cond);      
        $my_total_earning = $this->db->get()->row();

        $temp = date("i:s", $diff);

        $parts = explode(":", $temp);

        $interval_in_sec = ($parts[0] * 60) + $parts[1];

        if($interval_in_sec <= PRE_DRAW_INTERVAL){
            $pre_draw_number = $last_draw->draw_number . $last_draw->lucky_number;
            $pre_lucky_number =  $last_draw->lucky_number;
            $query = "SELECT draw_id, lucky_number, draw_number, draw_time FROM draws WHERE draw_number IS NOT NULL ORDER BY draw_id DESC LIMIT 1, 1";
            $last_draw = $this->db->query($query)->row();
            
            $last_draw_number = $last_draw->draw_number;
            $last_draw_lucky_number = $last_draw->lucky_number;
            $last_draw_id = $last_draw->draw_id;
        }

            
        $trigger = $interval_in_sec - PRE_DRAW_INTERVAL;
        $y=-1;
        $x=0;
        for($i=0, $count = count($entries); $i < $count; $i++){    
            if( $x % 9 == 0){
                $y++;
                $x=0;
            }        
            $this->db->select("profile_image, user_fbid")->from("users")->where('user_id', $entries[$i]->user_id);
            $buyer = $this->db->get()->row();
            if($buyer->profile_image != '' && !is_null($buyer->profile_image)){
                
                $profile_images[] = array("profile_image" => $buyer->profile_image, "coins" => $entries[$i]->user_investment);
            }
            else if($buyer->user_fbid){
                $profile_images[] = array("profile_image" => "https://graph.facebook.com/".$buyer->user_fbid."/picture?type=large", 
                                    "coins" => $entries[$i]->user_investment);
            }
            else {
                $profile_images[] = array("profile_image" => get_site_url() . "/profile-image.jpg"   , 
                                    "coins" => $entries[$i]->user_investment);
            }
            $x++;
            
        }

        
        $this->push(array(
            "status" => 1,
            "balance" => $user->balance,
            "draw_number" => $now_number,
            "time_left" => date("i:s", $diff),
            "draw_entries" => $entries,
       //     "last_draw_number" => $last_draw_number . $last_draw_lucky_number,
        //    "last_draw_lucky_number" => $pre_lucky_number,
            "my_earnings" => $my_earnings,
            "my_total_earning" => (int) $my_total_earning->total_earning,
           // "pre_draw_number" => $pre_draw_number,
            "pre_lucky_number" => $pre_lucky_number,
            "profile_images" => $profile_images,
           /* "interval_in_seconds" => $interval_in_sec,
            "trigger" => $trigger,
            "parts" => $parts*/
        )); 
    }

    function fresher(){
        $this->get_info();
    }

    function signup(){

        $errors = array();

            $this->load->model("Entity/Entity_User");

            $this->load->model("Message/Message_Error");

            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['phone'] = $this->input->post('phone');           
            $data['referrer'] = $this->input->post('referrer');           

            if(isset($_POST['first_name'])){
                $data['first_name'] = $this->input->post('first_name');
            }

            if(isset($_POST['last_name'])){
                $data['last_name'] = $this->input->post('last_name');
            }

            if(isset($_POST['confirm_password'])){
                $data['confirm_password'] = $this->input->post('confirm_password');
            }     

            if(isset($data["referrer"])){       

                $this->db->select("*")->from("users")->where("email", $data["referrer"]) ;
                $referrer = $this->db->get()->row();

                if(!is_object($referrer)){
                    $this->Message_Error->trigger("users", "referrer", "Your referrer is not valid");
                }

            }

         //   $this->db->query("COMMIT");
            $user_id = $this->Entity_User->sign_up($data);

            if(!$user_id){

                $errors = $this->Message_Error->get_by_entity("users");
                $message = "You can't register, errors found in your inputs \n\n";
                foreach($errors as $source => $messages){
                    $message .= implode(" \n", $messages) . " \n\n" ;
                }

                $json_data = array(
                    "status" => 0,
                    "user_id" => 0,      
                   // "errors" => $errors,
                    "message" => $message
                );
                $this->push($json_data);

            }            
            else {
                
                if(isset($data["referrer"]) && is_object($referrer)){

                    $transaction_id = $this->db->insert("transactions", array(
                        "user_id" => $referrer->user_id,
                        "amount" => 25,
                        "description" => "Referral bonus earned by referring user: ({$user_id}) {$email}",
                       // "user_payment_method_id" => $user_payment_method_id,
                        "transaction_type" => "Referal Bonus",
                        "transaction_status" => 1,
                        //"instructions" => ucfirst($instructions), 
                        "transaction_time" => date("Y-m-d H:i:s")
                    ));

                    $this->db->query("UPDATE users SET balance = balance + 25 WHERE user_id = {$data["referrer"]}");
                }    
                
                $credentials =  $this->Entity_User->login($data['email'], $data['password']);
                $message = "You have registered successfully";

                $json_data = array(
                    "status" => 1,
                    "user_id" => $credentials["user_id"],
                    "session_key" => $credentials["session_key"],      
                  //  "credentials" => $credentials,
                    "message" => $message
                );
                $this->push($json_data);
         
            }
      

    }
    
    function facebook_login(){

        $errors = array();

        $this->load->model("Entity/Entity_User");

        $this->load->model("Message/Message_Error");

        $data['email'] = $this->input->post('email');
        $data['session_key'] = $this->input->post('session_key');
        $data['accessToken'] = $this->input->post('accessToken');
        $data['expiresIn'] = $this->input->post('expiresIn');
        $data['sig'] = $this->input->post('sig');
        $data['secret'] = $this->input->post('secret');
        $data['picture'] = $this->input->post('pic');
        $data['userID'] = $this->input->post('userID');

        $user_id = $this->Entity_User->fb_register($data);

          if(!$user_id){

                $errors = $this->Message_Error->get_by_entity("users");
                $message = "You can't register, errors found in your inputs \n\n";
                foreach($errors as $source => $messages){
                    $message .= implode(" \n", $messages) . " \n\n" ;
                }

                $json_data = array(
                    "status" => 0,
                    "user_id" => 0,      
                   // "errors" => $errors,
                    "message" => $message
                );
                $this->push($json_data);

            }
            else {
                
                $credentials =  $this->Entity_User->fb_login($data['email'], $data['accessToken']);
                $message = "You have registered successfully";

                $json_data = array(
                    "status" => 1,
                    "user_id" => $credentials["user_id"],
                    "session_key" => $credentials["session_key"],      
                  //  "credentials" => $credentials,
                    "message" => $message
                );
                $this->push($json_data);
         
            }
      




    
    }

    function edit_profile(){
     
        

        $this->load->model("Entity/Entity_User");     

       auth_access_only();

       $user_info = get_credentials();
      
       $data = $errors = array();

       

    //   $this->Entity_User->force_login();

            $data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['phone'] = $this->input->post('phone');
            $data['gender'] = $this->input->post('gender');
            
            $data['city'] = $this->input->post('city');
            $data['state'] = $this->input->post('state');
            $data['country'] = $this->input->post('country');
            $data['date_of_birth'] = $this->input->post('date_of_birth');
            $data['profile_image'] = $this->input->post('profile_image');

            $this->db->where("user_id", (int) $user_info["user_id"] );            
            $updated = $this->db->update("users", $data);  
            
            $data["user_id"] = (int) $user_info["user_id"];
            $is_saved = $this->Entity_User->save_profile($data);

            $this->load->model("Message/Message_Error");

            $errors = $this->Message_Error->get_by_entity("users");

            if(count($errors) > 0) {
                $message = "Can not save information, errors found in user inputs \n\n";
                foreach($errors as $source => $messages){
                    $message .= implode(" \n", $messages) . " \n\n" ;
                }
                $this->push(array(
                    "status" => 0,
                    "message" => $message
                ));
            }

            $this->push(array(
                 "status" => 1,
                 "message" => "Profile information saved successfully"                
            ));
    }

    function login(){

        $errors = array();
        $this->load->model("Entity/Entity_User");        

            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $credentials = $this->Entity_User->login($email, $password);
            if( is_array($credentials) ){
               $this->push(array(
                    "status" => 1,
                    "user_id" => $credentials["user_id"],
               //     "credentials" => $credentials,
                    "session_key" => $credentials["session_key"],   
                    "message" => "You logged in successfully!"
                ));
                
            }
            else {
                $this->push(array(
                    "status" => 0,
                    "message" => "Your email or password is incorrect"
                ));
            }

    }

    function forgot_password(){

        $email = NULL;
        $this->load->model("Entity/Entity_User");
        $errors = array();
        $is_email_sent = 0;
        
        $email = $this->input->post("email");
        $is_email_sent = $this->Entity_User->forgot_password($email);
        $this->load->model("Message/Message_Error");
        $errors = $this->Message_Error->get_by_entity("users"); 

        if(count($errors) > 0) {
                $message = "";
                foreach($errors as $source => $messages){
                    $message .= implode(" \n", $messages) . " \n\n" ;
                }
                $this->push(array(
                    "status" => 0,
                    "message" => $message
                ));
            }

            $this->push(array(
                 "status" => 1,
                 "message" => "Your password is reset and sent on your email address!"
            ));
        
    }

    function change_password(){

        $this->load->model("Entity/Entity_User");

       // $this->Entity_User->force_login();
       auth_access_only();
        $errors = array();
        $success = 0;

        $confirm_new_password = $new_password = $current_password = "";

        

            $current_password = $this->input->post("current_password");
            $new_password = $this->input->post("new_password");
            $confirm_new_password = $this->input->post("confirm_new_password");

            if(!$this->Entity_User->is_auth()){
            $this->push(array(
                "status" => 2,
                "message" => "You need to login!"
            ));
        }

            $success = $this->Entity_User->change_password( $current_password, $new_password, $confirm_new_password);
            $this->load->model("Message/Message_Error");
            $errors = $this->Message_Error->get_by_entity("users");

        if(count($errors) > 0) {
                $message = "";
                foreach($errors as $source => $messages){
                    $message .= implode(" \n", $messages) . " \n\n" ;
                }
                $this->push(array(
                    "status" => 0,
                    "message" => $message
                ));
            }

            $this->push(array(
                 "status" => 1,
                 "message" => "Password changed successfully!"
            )); 
        
        
    }

    function password_recovery(){

        $this->load->model("Entity/Entity_User");

        $errors = array();
        $success = 0;

        $confirm_new_password = $new_password =  "";

        /*
        if(!$this->Entity_User->is_auth()){
            $this->push(array(
                "status" => 2,
                "message" => "You need to login!"
            ));
        }
        */

            $new_password = $this->input->post("new_password");
            $confirm_new_password = $this->input->post("confirm_new_password");

            $success = $this->Entity_User->reset_password($user_id, $new_password, $confirm_new_password);
            $this->load->model("Message/Message_Error");
            $errors = $this->Message_Error->get_by_entity("users");

            

        
    }

    function logout(){
        $this->load->model("Entity/Entity_User");
        $this->Entity_User->logout();

        $this->push(array(
            "status" => 1,
            "message" => "You logged out successfully!"
        ));

       // redirect_to("user", "login");
    }


}
