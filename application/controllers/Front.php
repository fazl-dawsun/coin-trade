<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends Root{

    function __construct($invoker = NULL){
        parent::__construct((is_object($invoker)) ? $invoker : $this);

        $user_info = get_credentials();

        $this->user_id = $user_info["user_id"];


    }

    function push($data = array()){
        if( (!is_array($data) && count($data) == 0)){
            if(!is_object($data)) $data = $this->data;
        }
        header("Content-Type: application/json");
        echo json_encode($data);
        exit;
    }

    function current_time(){
        $this->db->select("NOW() as my_current_time");
        $time = $this->db->get()->row()->my_current_time;

        $this->push(array(
            "mysql" => $time,
            "php" => date("Y-m-d H:i:s")
        ));
    }

    

}
