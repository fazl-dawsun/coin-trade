<?php 

defined("BASEPATH") OR exit("No direct script access allowed");

class Transaction extends Front{

    protected $user_id;

    function __construct($invoker = NULL){
        
        parent::__construct((is_object($invoker)) ? $invoker : $this);

        auth_access_only();     

    }

    function index(){

        $errors = array();       

        $this->load->model("Entity/Entity_Transaction");

        $user_info = get_credentials();

      //  $transactions = $this->Entity_Transaction->fetch_bulk(array("user_id"=>$user_info['user_id']));

        $this->db->select("*")->from("transactions")->where("user_id", $user_info['user_id'])->order_by("transaction_id", "DESC")->limit(100);

        $transactions = $this->db->get()->result();

        for($i=0, $count = count($transactions); $i < $count; $i++){
            $transactions[$i]->instructions = $transactions[$i]->description;
        }

        $this->push(array(
            "status" => 1,
            "transactions" => $transactions
        ));

    }

    function request($type /* "deposite"/"withdrawal"*/){
        $this->load->model("Entity/Entity_User");
        if(!$this->Entity_User->is_auth()){
            $this->push(array(
                "status" => 2,
                "message" => "You need to login"
            ));
        }
        if(!in_array($type, array("deposite", "withdrawal"))){
            $this->push(array(
                "status" => 0,
                "message" => "You need to send 'deposite' or 'withdrawal' for transaction type"
            ));
        }

        $amount = $this->input->post("amount");
        $user_payment_method_id = $this->input->post("user_payment_method_id");
        $instructions = ucfirst($this->input->post("instructions"));

        if(!$instructions){
            $this->push(array(
                "status" => 0,
                "message" => "Instructions are required"
            ));
        }

        if( (int) $amount < 500 ){
            $this->push(array(
                "status" => 0,
                "message" => "Amount should be number and greater than or equal to 500"
            ));
        }

        $user_info = get_credentials(); 
        /*
        $this->db->select("*")->from("user_payment_methods")->where(array(
            "user_id" => $user_info['user_id'],
            "user_payment_method_id" => $user_payment_method_id
        ));

        $result = $this->db->get();

        $payment_method = $result->row();

        if($type == "withdrawal" && !is_object($payment_method)){
            $this->push(array(
                "status" => 0,
                "message" => "user doesn't have the given payment method set"
            ));
        }           

        $this->db->select("*")->from("user_payment_methods")->where(array(
            "user_id" => 0,
            "user_payment_method_id" => $user_payment_method_id
        ));

        $result = $this->db->get();

        $admin_payment_method = $result->row();

        if($type == "deposite" && !is_object($admin_payment_method)){
            $this->push(array(
                "status" => 0,
                "message" => "Admin hasn't set the payment method you have entered "
            ));
        }   
        */

        $this->db->select("*")->from("users")->where("user_id", $user_info['user_id']);
        $result = $this->db->get();
        $current_user = $result->row();

        if($type == "withdrawal"){
            if($current_user->balance < $amount){
            $this->push(array(
                    "status" => 0,
                    "message" => "You have insuficicent balance"
                ));                
            }

            $amount *= (-1);
        }

        //$this->db->set("transaction_time", "NOW()", false);

        $description = ($type == "deposite") ?  "Sent request for deposite to admin" : "User withdraw amount from his account";
        
         $this->db->insert("transactions", array(
               // "sender_object" => "users",
                "user_id" => $user_info["user_id"],
                "amount" => $amount,
                "description" => $description,
            //    "user_payment_method_id" => $user_payment_method_id,
                "transaction_type" => $type,
                "instructions" => ucfirst($instructions), 
                "transaction_time" => date("Y-m-d H:i:s")
            ));
         
          $transaction_id = $this->db->insert_id(); 
           
         if($transaction_id){

             $this->load->library("my_email", null, "my_email");

             $this->my_email->from('cointrade@trailweb.site', get_site_name());         
              
             if($type == "deposite"){
                //$query = "UPDATE users SET balance = balance + $amount WHERE user_id = '{$user_info['user_id']}' ";

          //      http://www.trialweb.site/coin-trade//index.php/admin/transaction/view/602                
                
                
                $this->my_email->subject("You have desposite request pending at " . get_site_name());      

                $message = "You have a deposite request by the user: ({$current_user->user_id}) '{$current_user->first_name} {$current_user->last_name}', ";
              //  $message .= "for amount {$amount}, using payment method: {$admin_payment_method->bank_name}, a/c {$admin_payment_method->account_number}. <br>  ";
                $message .= "Description: {$instructions} <br>  ";
                $message .= "To check all details please <a href=\"" . get_route("admin/transaction", "view", array($transaction_id)) . "\">click here</a> <br>  ";

                $this->my_email->message($message);       

                $json_message = "Your request about deposite is sent successfully to admin!";         

             }
             else 
             if($type == "withdrawal" ){

                $query = "UPDATE users SET balance = balance + {$amount} WHERE user_id = '{$user_info['user_id']}' "; 
                $this->db->query($query);

                $this->my_email->subject("User has widraw coins at " . get_site_name());      

                $message = "User: ({$current_user->user_id}) '{$current_user->first_name} {$current_user->last_name}' has withdraw amount ";
                $message .= " {$amount}, using payment method: {$payment_method->bank_name}, a/c {$payment_method->account_number}. <br>  ";
                $message .= "Description: {$instructions} <br>  ";
                $message .= "To check all details please <a href=\"" . get_route("admin/transaction", "view", array($transaction_id)) . "\">click here</a> <br>  ";

                $this->my_email->message($message);         

                $json_message = "You withdraw successfully!";                

             }

             $this->my_email->to("accanouman@gmail.com");         
             
             $this->my_email->send();

             //$this->db->query("COMMIT");
             $this->push(array(
                 "status" => 1,                 
                 "transaction_id" => $transaction_id,                 
                 "message" => $json_message
             ));
         }          
         else {
            $this->push(array(
                 "status" => 0,                 
                 "message" => "Transaction failed"
             ));
         }  
        /*
        switch($type){
            case "deposite":
            $transaction_id = $this->db->insert("transactions", array(
                "sender_object" => "users",
                "sender_id" => $user_info["user_id"],
                "payment_method" => $payment_method,
                "transaction_type" => "deposite"
            ));
            break;

        }
        */
    }

    function transfer_coins(){

        $amount = (int) $this->input->post("amount");
        $receiver_id = $this->input->post("user_id");
        
        $this->db->select("user_id")->from("users")->where("user_id", $receiver_id);
        $result = $this->db->get();
        $user = $result->row();

        if( ! $amount > 0 ){
            $this->push(array(
                "status" => 0,
                "message" => "Please enter valid number of coins"
            ));
        }

        if(!is_object($user)){
            $this->push(array(
                "status" => 0,
                "message" => "Receiver user ID does not exist"
            ));
        }

        if($receiver_id == $this->user_id){
            $this->push(array(
                "status" => 0,
                "message" => "You can transfer coins to yourself"
            ));
        }


        $this->db->select("balance")->from("users")->where("user_id", $this->user_id);
        $result = $this->db->get();
        $sender = $result->row();

        if($sender->balance < $amount){
            $this->push(array(
                "status" => 0,
                "message" => "You have insufficient balance to transfer these coins",
                "balance" => $sender->balance
            ));
        }

        $this->db->query("START TRANSACTION");
        $this->db->query("SET AUTOCOMMIT = 0");
        ;        

        $this->db->select("*")->from("users")->where("user_id", $receiver_id);
        $receiver= $this->db->get()->row();

        $this->db->insert("transactions", array(
            "user_id" => $this->user_id,
            "amount" => $amount * (-1),
            "transaction_type" => "balance transfer",
            "transaction_status" => "1",
            "description" => "Coins transfered to User: ({$receiver->user_id}) '{$receiver->first_name} {$receiver->last_name}'",
            "transaction_time" => date("Y-m-d H:i:s")
        ));

        $main_transaction_id = $this->db->insert_id();

        if(!$main_transaction_id){
            $this->push(array(
                "status" => 0,
                "message" => "Fail to start transaction"
            ));
        }

        $this->db->query("UPDATE users SET balance = balance - {$amount} WHERE user_id = {$this->user_id} ");

        $this->db->set("transaction_time", "NOW()", false);

        $this->db->select("*")->from("users")->where("user_id", $this->user_id);
        $sender = $this->db->get()->row();

        $this->db->insert("transactions", array(
            "user_id" => $receiver_id,
            "amount" => $amount,
            "transaction_type" => "balance transfer",
            "transaction_status" => "1",
            "description" => "Coins received from User: ({$sender->user_id}) '{$sender->first_name} {$sender->last_name}' ",
            "parent_id" => $main_transaction_id
        )); 

        $this->db->query("UPDATE users SET balance = balance + {$amount} WHERE user_id = {$receiver_id} ");

        $this->db->query("COMMIT");


        $this->load->library("my_email", null, "my_email");

        $this->my_email->from('cointrade@trailweb.site', get_site_name());         
        $this->my_email->to($sender->email);         

        $this->my_email->subject("You have successfully transfered coins to user ({$receiver->user_id}) at  '{$receiver->first_name} {$receiver->last_name}' " . get_site_name());      

        $message = "You have transfered coins to user ({$receiver->user_id}) '{$receiver->first_name} {$receiver->last_name}' ";

        $this->my_email->message($message);

        $this->my_email->send();

        $this->my_email->to($receiver->email);         

        $this->my_email->subject("You have successfully recieved coins from user ({$sender->user_id}) '{$sender->first_name} {$sender->last_name}' at " . get_site_name());      

        $message = "You have transfered coins to user ({$receiver->user_id}) ({$sender->user_id}) '{$sender->first_name} {$sender->last_name}' ";

        $this->my_email->message($message);

        $this->my_email->send();


        $this->push(array(
            "status" => 1,
            "message" => "Coins transfered successfully!"
        ));
        

    }



}
    
    
    