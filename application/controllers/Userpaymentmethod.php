<?php 

defined("BASEPATH") OR exit("No direct script access allowed");

class Userpaymentmethod extends Front{

    function __construct($invoker = NULL){
        parent::__construct((is_object($invoker)) ? $invoker : $this);
        auth_access_only();
    }

    function index(){

        $errors = array();       

        $this->load->model("Entity/Entity_UserPaymentMethod");

        $user_info = get_credentials();

        //$user_payment_methods = $this->Entity_UserPaymentMethod->fetch_bulk(array("user_id"=>$user_info['user_id']));

        $this->db->select("*")->from("user_payment_methods")->where("user_id", $user_info["user_id"])->order_by("user_payment_method_id", "DESC");

        $user_payment_methods = $this->db->get()->result();

        $this->push(array(
            "status" => 1,
            "user_payment_methods" => $user_payment_methods
        ));

    }

    function add($method_name = NULL){

        $errors = array();

            $this->load->model("Entity/Entity_UserPaymentMethod");

            $user_info = get_credentials();
			
			//$data["user_id"] = $user_info["user_id"];
            if($method_name){
                $data["method_name"] = $method_name;
            }
            else $data["method_name"] = $this->input->post("method_name");
            $data["account_number"] = $this->input->post("account_number");
            $data["user_id"] = $user_info["user_id"];
            $data["bank_name"] = $this->input->post("bank_name");

        if($data["method_name"] == "bank"){
            
			$data["account_title"] = $this->input->post("account_title");
			
			$data["bank_address"] = $this->input->post("bank_address");
			$data["branch_code"] = $this->input->post("branch_code");
        }    
        else if($data["method_name"] == "mobile"){			
			$data["cnic"] = $this->input->post("cnic");
        }     
        else {
            $this->push(array(
                    "status" => 0,
                    "message" => "Allowed payment methods are 'bank' and 'mobile'",
                    "data" => $data,
                    "user_payment_method_id" => 0
                ));
        }

			
       $user_payment_method_id = $this->Entity_UserPaymentMethod->add($data);

            if(!$user_payment_method_id){
                $this->load->model("Message/Message_Error");
                $errors = $this->Message_Error->get_by_entity("user_payment_methods");
                $err_msg = '';
                foreach($errors as $messages){
                    $err_msg .= implode("\n", $messages);
                }
                $this->push(array(
                    "status" => 0,
                    "message" => "Unable to save data  $err_msg",
                    "user_payment_method_id" => 0
                ));
                
            }
            else {
                $this->push(array(
                    "status" => 1,
                    "message" => "Payment method added successfully",
                    "user_payment_method_id" => $user_payment_method_id
                ));
                
            }

       

    }


    function deposite_method(){
        
        $errors = array();       

        $this->load->model("Entity/Entity_UserPaymentMethod");

        $user_info = get_credentials();

        //$admin_payment_methods = $this->Entity_UserPaymentMethod->fetch_bulk(array("user_id"=>0));

        $this->db->select('user_payment_method_id, method_name, account_number, account_title, bank_name, bank_address, branch_code, cnic')
                ->from('user_payment_methods')->where('user_id', 0);

        $admin_payment_methods = $this->db->get()->result();        

        $this->push(array(
            "status" => 1,
            "deposite_methods" => $admin_payment_methods
        ));
    }

    function edit(){

        $errors = array();

        

            $this->load->model("Entity/Entity_UserPaymentMethod");
			$data["user_payment_method_id"] = $this->input->post("user_payment_method_id");
			$data["user_id"] = $this->input->post("user_id");
			$data["method_name"] = $this->input->post("method_name");
			$data["account_number"] = $this->input->post("account_number");
			$data["account_title"] = $this->input->post("account_title");
			$data["bank_name"] = $this->input->post("bank_name");
			$data["bank_address"] = $this->input->post("bank_address");
			$data["bank_code"] = $this->input->post("bank_code");
			$data["cnic"] = $this->input->post("cnic");
			$user_id = $this->Entity_UserPaymentMethod->modify($data);

            if(!$user_payment_method_id){

            }
            else {

            }

        

    }

}
    
    
    