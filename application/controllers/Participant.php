<?php 

defined("BASEPATH") OR exit("No direct script access allowed");

class Participant extends Front{

    function __construct($invoker = NULL){
        parent::__construct((is_object($invoker)) ? $invoker : $this);
        auth_access_only();
    }

    function index(){

        $errors = array();       

        $this->load->model("Entity/Entity_Participant");

        $user_info = get_credentials();

        //$participants = $this->Entity_Participant->fetch_bulk(array("user_id"=>$user_info["user_id"]));

        $this->db->select("*")->from("participants")->where("user_id", $user_info["user_id"])->order_by("participant_id", "DESC");

        $participants = $this->db->get()->result();

        $this->push(array(
            "status" => 1,
            "participants" => $participants
        ));

    }

    function current_entries(){
        
        $user_info = get_credentials();

        $now = date("Y-m-d H:i:s");
        $query = "SELECT draw_time, lucky_number, coins FROM participants WHERE draw_time > '{$now}' 
                            AND user_id = '{$user_info['user_id']}' ORDER BY participant_id DESC";
        $entries = $this->db->query($query)->result();
        
        $this->push(array(
            "status" => 1,
            "entries" => $entries
        ));
    }

    function add(){

        $errors = array();

        $user_info = get_credentials();        

            $this->load->model("Entity/Entity_Participant");
			
			$data["user_id"] = $user_info["user_id"];
			$data["coins"] = $this->input->post("coins");
			$data["lucky_number"] = $this->input->post("lucky_number");

            if(!preg_match("#^\d$#", $data["lucky_number"])){
                $this->push(array(
                    "status" => 0,
                    "message" => "Please enter lucky number 0-9"
                ));
            } 

            if(  (int) $data["coins"] < 10){
                $this->push(array(
                    "status" => 0,
                    "message" => "Please use atleast 10 coins"
                ));
            }

            $this->db->select("balance")->from("users")->where("user_id", $user_info["user_id"]);
            $result = $this->db->get();
            $row = $result->row();

            if($data["coins"] > $row->balance){
                $this->push(array(
                    "status" => 0,
                    "message" => "Your balance is insufficient",
                    "participant_id" => 0
                ));
            }

			$participant_id = $this->Entity_Participant->add($data);

            if(!$participant_id){
                $this->load->model("Message/Message_Error");
                $errors = $this->Message_Error->get_by_entity("participants");            

                
                $err_msg = '';
                foreach($errors as $messages){
                    $err_msg .= implode("\n", $messages);
                }
                $this->push(array(
                    "status" => 0,
                    "message" => "Unable to save data  $err_msg",
                    "participant_id" => 0
                ));
                
            }
            else {

                $this->push(array(
                    "status" => 1,
                    "message" => "Lucky number {$data['lucky_number']} bought successfully.",
                    "participant_id" => $participant_id
                ));
            }

        

       

    }

    function edit(){

        $errors = array();

        if($this->input->post("action") == "modify_participant"){

            $this->load->model("Entity/Entity_Participant");
			$data["participant_id"] = $this->input->post("participant_id");
			$data["user_id"] = $this->input->post("user_id");
			$data["coins"] = $this->input->post("coins");
			$data["lucky_number"] = $this->input->post("lucky_number");
			$data["draw_time"] = $this->input->post("draw_time");
			$data["winner"] = $this->input->post("winner");
			$data["reward"] = $this->input->post("reward");
			$data["draw_id"] = $this->input->post("draw_id");
			$user_id = $this->Entity_Participant->modify_($data);

            if(!$participant_id){
                
            }
            else {

            }

        }

       

    }

}
    
    
    