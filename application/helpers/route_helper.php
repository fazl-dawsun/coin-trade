<?php

$ci = &get_instance(); 

$ci->load->helper('url');

function get_site_url(){
    global $ci;
    return "http://localhost/coin-trade/";
    return base_url();
}

function get_route($controller="index", $action = "index", array $params = array()){
    return  get_site_url(). 'index.php/' . $controller . '/' . $action . "/" . implode("/", $params);
}

function redirect_to($controller="index", $action = "index", array $params = array()){
    $url = get_route($controller, $action,  $params  );
    redirect($url);
}

function redirect($url){
    header("Location: " . $url);
    exit;
} 

function get_admin_url(){
    return get_site_url() . "admin";
}

function get_admin_route($controller, $action = "index", $params = array()){
    $route =  get_site_url() . "/index.php/admin/" .$controller.'/'. $action;
    if(count($params)>0) $route .= "/" . implode("/", $params);
    return $route;
}




