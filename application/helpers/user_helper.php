<?php

function is_logged_in(){
    $ci = get_main_instance();
    $ci->load->model("Entity/Entity_User");
    return $ci->Entity_User->is_auth(); 
}

function auth_access_only(){
    $ci = get_main_instance();
    $ci->load->model("Entity/Entity_User");
    $ci->Entity_User->force_login();
}

function get_currentuser($field = NULL){
    $ci = get_main_instance();
    $ci->load->model("Entity/Entity_User");
    return $ci->Entity_User->get_current_user($field);
}

function is_admin_user(){
    $ci = get_main_instance();
    $ci->load->model("Entity/Entity_Admin");

    return $ci->Entity_Admin->is_admin_user();
} 

function force_admin_login(){
    $ci = get_main_instance();
    $ci->load->model("Entity/Entity_Admin");

    $ci->Entity_Admin->force_login();
}