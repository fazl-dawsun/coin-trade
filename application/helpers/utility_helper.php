<?php

define("DRAW_INTERVAL", 60 * 9);
define("PRE_DRAW_INTERVAL", 30 );
define("SYSTEM_NUMBER", 677);


function get_main_instance(){
    return get_instance();
}

function get_post( $name ){
    $ci = get_main_instance();
    return $ci->input->post( $name );
}

function create_slug($text, $table, $column, $debug = 0){    
    
    $slug = strtolower(preg_replace("#[^A-Za-z\s\-_]#", "-", $text));
    $ci = get_main_instance();
 
        
        $ci->db->select("*")->from($table)->where($column, $slug)->limit(1);
        $row = $ci->db->get()->row_array();
        if(!is_array($row)) return $slug;
        else return "";
        
    
}

function text_2_number($text){
    return preg_replace("#[^0-9\.]#", "", $text);
}

function number_to_currency($amount){
    return number_format($amount, 1);
}

function get_site_email(){
    return "fazl.dawsun@gmail.com";
}

function get_site_name(){
    return "Mufta Online";
}

function get_site_info(){
    $site_info["name"] = get_site_name();
    $site_info["email"] = get_site_email();
    return $site_info;
}

function manage_account_from_social_platforum($info){
    echo '<pre>'; print_r($info); echo '</pre>';
    exit;
}

function currency_format($amount){
     return $amount;   
}


function get_random_text($char_limit = 6){

	$chars = array(0,1,2,3,4,5,6,7,8,9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", 
						"r", "s", "t", "u", "v", "w", "x", "y", "z");

	$text = "";

	for($i=0; $i < $char_limit; $i++){
		$index = rand(0, count($chars) - 1);
		$text .= $chars[$index];
	}

	return $text;
}

function get_credentials(){
        return array(
            "user_id" => (int) @$_SERVER['HTTP_USERID'],
            "session_key" => @$_SERVER['HTTP_KEY']
        );
    }

function get_draw_number($number = 0, $time=NULL, $system_number = 999, $debug = false){

    $ci = get_main_instance();

    $query = "SELECT lucky_number, draw_number, draw_time FROM draws WHERE draw_number IS NOT NULL ORDER BY draw_id DESC LIMIT 1";
    $last_draw = $ci->db->query($query)->row();
    $draw_number = $last_draw->draw_number;

    $draw_number += SYSTEM_NUMBER;

    if($draw_number > 99999999) $draw_number = SYSTEM_NUMBER;

    return $draw_number;

    /*
    $number = (int) $number;
    if(!$time){
        $date = date("2017-04-03 H:i:s");
        $linux_time = strtotime($date);
        $now = time();
        $time = $now - $linux_time; 
        $time = 0;       
    } 


    
    $number = $system_number / DRAW_INTERVAL ; 
    
   // $number = floor($number * $multiple);


    
    if($number > 99999) $number = 0;
    return $number;

    */


}    

/*
function next_draw($insert = false){
    $ci = get_main_instance();
    $ci->db->select("*")->from("draws")->order_by("draw_id", "desc");
    $last_draw = $ci->db->get()->row();
    if(!is_object($last_draw)){

    }
    else {
        $last_draw->
    }
}
*/


function get_user_ip(){

    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }
    else{
        $ip = $remote;
    }

    return $ip;
}