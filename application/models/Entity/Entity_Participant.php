<?php

class Entity_Participant extends Entity{

    function __construct(){
        parent::__contruct($this);
    }    

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Participant");

			
			$ci->Data_Participant->user_id = $data["user_id"];
			$ci->Data_Participant->coins = $data["coins"];
			$ci->Data_Participant->lucky_number = $data["lucky_number"];

            $this->db->select("*")->from("draws")->order_by("draw_id", "desc")->limit(1);
            $draw = $this->db->get()->row();
            $draw_time = $draw->draw_time;
            if(!empty($draw->draw_number)){
                $temp = strtotime($draw_time) + DRAW_INTERVAL;
                $draw_time = date("Y-m-d H:i:s", $temp);
            }    

			$ci->Data_Participant->draw_time = $draw_time;
			

        $participant_id = $ci->Data_Participant->insert();

        if($participant_id){
            /*$this->db->select("*")->from("participants")->where("participant_id", $participant_id);
            $amount = $this->db->get()->row()->coins;
            */
           // $this->db->set("transaction_time", "NOW()", false);
            $this->db->insert("transactions", array(
                "user_id" => $data["user_id"],
                "amount" => (-1) * $data["coins"],
                "description" => "Bought lucky number: {$data["lucky_number"]}",
                "transaction_type" => "buy lucky number",
                "transaction_status" => 1,
                "transaction_time" => date("Y-m-d H:i:s")
            ));

            $query = "UPDATE users SET balance = balance - {$data['coins']} WHERE user_id = '{$data['user_id']}'";
            //echo $query;exit;
            $this->db->query($query);
            $this->db->query("COMMIT");
            return $participant_id;
        }

        
    }

    function distribute_prizes(){
        $ci = get_main_instance();
        $ci->load->model("Entity/Entity_Draw");

        $query = "SELECT * FROM draws WHERE lucky_number >= 0 AND draw_time < '" .date("Y-m-d H:i:s") . "' ORDER BY draw_id DESC LIMIT 1";
        $last_draw = $this->db->query($query)->row();

        $ci->Entity_Draw->distripute_prize_money($last_draw->draw_id);

        /*

        $query = "SELECT * FROM participants WHERE (draw_id IS NULL OR draw_id = 0) AND draw_time < '" . date("Y-m-d H:i:s") . "'";
        $query = "SELECT * FROM participants WHERE winner = 0 AND draw_time < '" . date("Y-m-d H:i:s") . "'";

        $participants = $this->db->query($query)->result();
        for($i=0, $count = count($participants); $i < $count; $i++){
            $this->db->select("*")->from("draws")->where("draw_time", $participants[$i]->draw_time);
            $draw = $this->db->get()->row();
            if(is_object($draw)){
                $ci->Entity_Draw->distripute_prize_money($draw->draw_id);
            }
            else {
                $this->db->where("participant_id", $participants[$i]->participant_id);
                $this->db->update("participants", array(
                    "draw_id" => -1
                ));
            }
            
        }
        /*
        header("Content-Type: application/json");
        echo json_encode(array(
            "status" => 1,
            "participants" => $participants,
            "draw" => $draw
        ));
        exit;
        */
        
    }
    
    function remove($participant_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Participant");

        return $ci->Data_Participant->delete("participants", array("participant_id"=>$participant_id));
    }

    function fetch($participant_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("participants")->where("participant_id", $participant_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("participants");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    
}       
