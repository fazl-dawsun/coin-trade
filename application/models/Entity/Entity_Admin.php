<?php

class Entity_Admin extends Entity{

    function __construct(){
        parent::__contruct($this);
        if(!session_id()) session_start();
    }

    function login($email, $password){

        $this->db->select("admin_id, email")->from("admins")->where(array(
            "email" => $email,
            "password" => md5($password)
        ));

        $admin = $this->db->get()->row_array();

        if(isset($admin) && is_array($admin)){
            $_SESSION["admin_user"] = $admin;            
            return true;
        }
        else return false;

    }

    function logout(){
        session_destroy();
    }

    function is_auth(){
            //$this->load->library('session');
            //$user = $this->session->userdata( "current_user" );

          // if(!isset($_SESSION)) {
          //   //  return false;
          //   session_start();
          //   }
          //   else{

                return isset($_SESSION["admin_user"]) && is_array($_SESSION["admin_user"]);
      //      }


        }

    function force_login(){

        if(!$this->is_admin_user()){
            redirect_to("adminaccount", "login");
        }

    }

    function is_admin_user(){
        return isset($_SESSION["admin_user"]);
    }

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Admin");

			$ci->Data_Admin->admin_id = $data["admin_id"];
			$ci->Data_Admin->email = $data["email"];
			$ci->Data_Admin->password = $data["password"];


        return $ci->Data_Admin->insert();
    }

    function modify(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Admin");

			$ci->Data_Admin->admin_id = $data["admin_id"];
			$ci->Data_Admin->email = $data["email"];
			$ci->Data_Admin->password = $data["password"];


        return $ci->Data_Admin->update();
    }

    function remove($admin_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Admin");

        return $ci->Data_Admin->delete("admins", array("admin_id"=>$admin_id));
    }

    function fetch($admin_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("admins")->where("admin_id", $admin_id);
        return $this->db->get()->row();
    }

    function change_password($old, $new, $confirm){
        
         $ci = get_main_instance();
         $user = $_SESSION["admin_user"];
         $ci->db->select("password")->from("admins")->where("admin_id", $user['admin_id']);
         $row = $ci->db->get()->row();

         $password = $row->password;

         $ci->load->model("Message/Message_Error");

         if(md5($old) != $password){
             $ci->Message_Error->trigger("admins", "current_password", "Your current password is incorrect"  );
             return 0;
         }

         if(strlen($new)<6){
             $ci->Message_Error->trigger("admins", "new_password", "Your new password must be atleast of 6 characters"  );
             return 0;
         }

         if($new != $confirm){
             $ci->Message_Error->trigger("admins", "confirm_new_password", "Your new password mismatch at confirm"  );
             return 0;
         }

       //  echo '<pre>'; print_r($user); print_r($_POST); exit; 

         $this->db->where("admin_id", $user['admin_id']);
         $this->db->update("admins", array("password"=> md5($new)));
         return $this->db->affected_rows();

    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("admins");
        if( count($cond) > 0){
            $this->db->where($cond);
        }
        return $this->db->get()->result();
    }
}
