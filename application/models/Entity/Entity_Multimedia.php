<?php

class Entity_Multimedia extends Entity{
/*
    function __construct(){
        parent::__contruct($this);
    }    
*/
    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Multimedia");
			
		$ci->Data_Multimedia->multimedia_name = $data["multimedia_name"];
		$ci->Data_Multimedia->multimedia_slug = $data["multimedia_slug"];
		$ci->Data_Multimedia->multimedia_type = $data["multimedia_type"];
        if(isset($data["multimedia_location"])) {
            $ci->Data_Multimedia->multimedia_location = $data["multimedia_location"];
        }        
		$ci->Data_Multimedia->multimedia_content = $data["multimedia_content"];
		//$ci->Data_Multimedia->upload_date = $data["upload_date"];

        $ci->db->set("upload_date", "NOW()", false);	

        return $ci->Data_Multimedia->insert();
    }

    function upload($file_index){

        if(is_file($_FILES[$file_index]["tmp_name"])){

            $data["multimedia_name"] = $_FILES[$file_index]["name"];
            $data["multimedia_slug"] = $_FILES[$file_index]["name"];
            $data["multimedia_type"] = $_FILES[$file_index]["type"];
            $data["multimedia_content"] = file_get_contents($_FILES[$file_index]["tmp_name"]);

            return $this->add($data);
            
        }
        
    }

    function re_upload($file_index, $multimedia_id){

        if(is_file($_FILES[$file_index]["tmp_name"])){

            $data["multimedia_id"] = $multimedia_id;
            $data["multimedia_name"] = $_FILES[$file_index]["name"];
            $data["multimedia_slug"] = $_FILES[$file_index]["name"];
            $data["multimedia_type"] = $_FILES[$file_index]["type"];
            $data["multimedia_content"] = file_get_contents($_FILES[$file_index]["tmp_name"]);

            return $this->modify($data);
            
        }
        
    }

    function upload_bulk($file_index){
        $multimedia_ids = array();

         

        for($i=0, $count = count($_FILES[$file_index]["tmp_name"]); $i < $count; $i++){

            $data = array();

            if(is_file($_FILES[$file_index]["tmp_name"][$i])){

                $data["multimedia_name"] = $_FILES[$file_index]["name"][$i];
                $data["multimedia_slug"] = $_FILES[$file_index]["name"][$i];
                $data["multimedia_type"] = $_FILES[$file_index]["type"][$i];
                $data["multimedia_content"] = file_get_contents($_FILES[$file_index]["tmp_name"][$i]);

                $multimedia_ids[] = $this->add($data);
                
            }            

        }

        return $multimedia_ids;
        
    }



    function show($multimedia_id){
        $file = $this->fetch($multimedia_id);
        if(is_object($file)){
            if($file->multimedia_type != "embed video"){
				header("Content-Type: {$file->multimedia_type}");
				header('Content-Disposition: inline; filename="'.$file->multimedia_name.'"');
				echo $file->multimedia_content;
				exit;
			}
        }
    }

    function modify(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Multimedia");

			$ci->Data_Multimedia->multimedia_id = $data["multimedia_id"];
			$ci->Data_Multimedia->multimedia_name = $data["multimedia_name"];
			$ci->Data_Multimedia->multimedia_slug = $data["multimedia_slug"];
			$ci->Data_Multimedia->multimedia_type = $data["multimedia_type"];
			//$ci->Data_Multimedia->multimedia_location = $data["multimedia_location"];
			$ci->Data_Multimedia->multimedia_content = $data["multimedia_content"];
		//	$ci->Data_Multimedia->upload_date = $data["upload_date"];
			

        return $ci->Data_Multimedia->update();
    }

    function remove($multimedia_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Multimedia");

        return $ci->Data_Multimedia->delete("multimedia", array("multimedia_id"=>$multimedia_id));
    }

    function fetch($multimedia_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("multimedia")->where("multimedia_id", $multimedia_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("multimedia");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    
}       
