<?php

class Entity_User extends Entity{

    
    function sign_up(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_User");
        $ci->load->model("Message/Message_Error");

        $ci->Data_User->email = $data['email'];

      //  echo json_encode($data); exit;

        $ci->Data_User->password = $data['password'];
        $ci->Data_User->phone = $data['phone'];

        if(isset($data['first_name'])) $ci->Data_User->first_name = $data['first_name'];
        if(isset($data['last_name'])) $ci->Data_User->last_name = $data['last_name'];

        if(isset($data['referrer'])){

            $this->db->select("*")->from("users")->where("user_id", $data["referrer"]) ;
            $referrer = $this->db->get()->row();

            $ip_address = get_user_ip();

            $this->db->select("*")->from("access_log")->where(array(
                "user_id" => $referrer->user_id,
                "ip_address" => $ip_address
            ));

            $log = $this->db->get();

            if(!is_object($referrer)){
                 $this->Message_Error->trigger("users", "referrer", "Your referrer is not valid");
            }
            else if(get_user_ip() == $referrer){
                $ci->Data_User->referrer_id = $data['referrer'];
            } 

        } 



        $ci->db->select("*")->from("users")->where("email", $this->input->post('email'));
        $user = $ci->db->get()->row();

        if(is_object($user)){
            $this->Message_Error->trigger("users", "email", "Email already exist in our records");
        }
     
        if(isset($data['confirm_password']) && $data['password'] != $data['confirm_password']){            
            $ci->Message_Error->trigger("users", "confirm_password", "Your password mismatches at confirm" );
        }

        if(isset($data['confirm_password']) && $data['password'] != $data['confirm_password']){            
            $ci->Message_Error->trigger("users", "confirm_password", "Your password mismatches at confirm" );
        }
      
       $ci->db->set("date_of_join", "NOW()", false);
        $user_id = $ci->Data_User->insert();

        $ci->db->select("*")->from("users")->where("user_id", $user_id);
        $user = $this->db->get()->row_array();

        if(isset($user) && is_array($user)){                        
            return $user["user_id"];
        }
    }


    function fb_register(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_User");
        $ci->load->model("Message/Message_Error");

        $ci->Data_User->email = $data['email'];

      //  echo json_encode($data); exit;

        if(isset($data['first_name'])) $ci->Data_User->first_name = $data['first_name'];
        if(isset($data['last_name'])) $ci->Data_User->last_name = $data['last_name'];

        $ci->db->select("*")->from("users")->where("email", $this->input->post('email'));
        $user = $ci->db->get()->row();

        if(is_object($user)){
           // $this->Message_Error->trigger("users", "email", "Email already exist in our records");
            return $user->user_id;
        }
    
        if(isset($data['session_key'])){
            $ci->Data_User->session_key = $data['session_key'];
            
            $ci->Data_User->access_token = md5($data['accessToken']);
            
            $ci->Data_User->expiresIn = $data['expiresIn'];
            
            $ci->Data_User->sig = $data['sig'];
            
            $ci->Data_User->secret = $data['secret'];
            
            $ci->Data_User->profile_image = $data['picture'];
            $ci->Data_User->imagedata = $data['picture'];
            

            $ci->Data_User->user_fbid = $data['userID'];
        }



        $ci->db->set("date_of_join", "NOW()", false);
        $user_id = $ci->Data_User->insert();

        $ci->db->select("*")->from("users")->where("user_id", $user_id);
        $user = $this->db->get()->row_array();

        if(isset($user) && is_array($user)){                        
            return $user["user_id"];
        }
    }

    function logout(){

        $info = get_credentials();

        $this->db->where("user_id", $info["user_id"]);
        $this->db->set("session_key", "NULL", false);
        $this->db->update("users");
    }

 
    function save_profile( array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_User");

        $ci->Data_User->user_id = (int) $data['user_id'];

        $ci->Data_User->first_name = $data['first_name'];
        $ci->Data_User->last_name = $data['last_name'];

        $ci->Data_User->city = $data['city'];
        $ci->Data_User->state = $data['state'];
        $ci->Data_User->country = $data['country'];

        $ci->Data_User->phone = $data['phone'];
        
        $ci->Data_User->date_of_birth = $data['date_of_birth'];
        $ci->Data_User->gender = $data['gender'];

        if( isset( $data["profile_image"] ) ){
            $ci->Data_User->profile_image = $data['profile_image'];
        }       

        $is_saved = $ci->Data_User->update();

    }




    function login($email, $password){
        if(!session_id()) session_start();

        $query = "SELECT user_id, email FROM users WHERE email = '{$email}' AND password = '" . md5($password) . "'";

        $user = $this->db->query($query)->row_array();

        if(is_array($user)){

            

            $session_key = get_random_text(12);

            $this->db->where("user_id", $user["user_id"]);
            $this->db->update("users", array("session_key"=> md5($session_key)));

            return array(
                "user_id" => $user["user_id"],
                "session_key" => $session_key,
            );

        }
        else {
            return false;
        }

    }

    function fb_login($email, $access_token){
         if(!session_id()) session_start();

        $query = "SELECT user_id, email FROM users WHERE email = '{$email}'";

        $user = $this->db->query($query)->row_array();

        if(is_array($user)){

            

            $session_key = get_random_text(12);

            $this->db->where("user_id", $user["user_id"]);
            $this->db->update("users", array("session_key"=> md5($session_key)));

            return array(
                "user_id" => $user["user_id"],
                "session_key" => $session_key,
            );

        }
        else {
            return false;
        }

    }

    function change_password($old, $new, $confirm){

         
         $ci = get_main_instance();

         $user = get_credentials();;

         $ci->db->select("password")->from("users")->where("user_id", $user['user_id']);
         $row = $ci->db->get()->row();

         $password = $row->password;

         $ci->load->model("Message/Message_Error");

         if(md5($old) != $password){
             $ci->Message_Error->trigger("users", "current_password", "Your current password is incorrect"  );
             return 0;
         }

         if(strlen($new)<6){
             $ci->Message_Error->trigger("users", "new_password", "Your new password must be atleast of 6 characters"  );
             return 0;
         }
         else if(preg_match("[\s]", $new)){
            $ci->Message_Error->trigger("users", "new_password", "Space is not allowed in password"  );
             return 0;
         }

         if($new != $confirm){
             $ci->Message_Error->trigger("users", "confirm_new_password", "Your new password does not match with confirm password"  );
             return 0;
         }

         $query = "UPDATE users SET password = " . md5($new) . " WHERE user_id = '{$user['user_id']}'";

         //echo json_encode(array("query"=> $query)); exit;

         $this->db->where("user_id", $user['user_id']);
         $this->db->update("users", array("password"=> md5($new)));
         return $this->db->affected_rows();

    }

    function reset_password($user_id, $new, $confirm){

         $ci = get_main_instance();

         $ci->db->select("*")->from("users")->where("user_id", $user_id);
         $row = $ci->db->get()->row();

         $ci->load->model("Message/Message_Error");

         if(!is_object($row)){
             $ci->Message_Error->trigger("users", "users", "Invalid User"  );
             return 0;
         }

         if(strlen($new)<6){
             $ci->Message_Error->trigger("users", "new_password", "Your new password must be atleast of 6 characters"  );
             return 0;
         }

         if($new != $confirm){
             $ci->Message_Error->trigger("users", "confirm_new_password", "Your new password mismatch at confirm"  );
             return 0;
         }

         $this->db->where("user_id", $user_id);
         $this->db->update("users", array("password"=> md5($new)));
         return $this->db->affected_rows();

    }

    function forgot_password($email){
        $ci = get_main_instance();

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
              $this->Message_Error->trigger("users", "email", "Please enter valid email address");
        }

        $user = $this->get_row_by("email", $email);        

        if(!is_array($user)){
            $ci->load->model("Message/Message_Error");
            $ci->Message_Error->trigger("users", "email", "We couldn't find user account associated with your email, Would you like to sign up instead?"  );
            return 0;
        }
        else{
          // $link = '<a href="'.get_route("user", "password_recovery", array($user["user_id"])).'">click here</a>';

           $password = get_random_text(12);

           $ci->db->where("user_id", $user["user_id"]);
           $ci->db->update("users", array("password"=>md5($password)));

           $message = "We have reset your password and your new password is <strong>'$password'</strong>";

         

           $headers[] = 'MIME-Version: 1.0';
           $headers[] = 'Content-type: text/html; charset=iso-8859-1';
           $headers[] = 'To: ' . $user["first_name"] . ' <' . $user["email"] . '>';
           $headers[] = 'From: '.get_site_name().' <cointrade@trailweb.site>';

           $ci->load->library("my_email");
           $ci->my_email->from('cointrade@trailweb.site', get_site_name());
           $ci->my_email->to($user["email"]);
            
           $ci->my_email->subject("Password Recovery at " . get_site_name());
           $ci->my_email->message($message);

           return $ci->my_email->send();

           //return mail($user["email"], "Password Recovery  at " . get_site_name(), $message, implode("\r\n", $headers));

        }

    }

    function get_row_by($field, $value){
        $this->db->select("*")->from("users")->where($field, $value);
        $user = $this->db->get()->row_array();
        return $user;
    }

    function is_auth(){

        $ci = get_main_instance();

        $info = get_credentials();

        //extract($info);

        $user_id = $info["user_id"];
        $session_key = $info["session_key"];
        
        /*
        echo json_encode(array(
            "user_id" => $user_id,
            "session_key" => $session_key,
            "info" => $info
        ));
        exit;
        */

        if(!$user_id || !$session_key){
            return false;
        }

        $this->db->select("block")->from("users")->where(array(
            "user_id" => $user_id,
            "session_key" => md5($session_key)
        ));

        $result = $this->db->get();

        $user = $result->row(); 

        /*
        echo json_encode(array(
            "user" => $user
        ));
        exit;
        */
        


        if(!is_object($user)){
            header("Content-Type: application/json");
            echo json_encode(array(
                "status" => 2,
                "message" => "User not found"
            ));
            exit;
        }

        if($user->block == 1) {
            header("Content-Type: application/json");
            echo json_encode(array(
                "status" => 3,
                "message" => "You are blocked/suspended by the admin"
            ));
            exit;
        }


        
        return true;
        
    }

    function force_login(){
        if( !$this->is_auth() ){
            header("Content-Type: application/json");
            echo json_encode(array(
                "status" => 2,
                "message" => "You need to login"
            ));
            exit;
        }
    }

    function get_current_user($field=NULL){
        
    }
}
