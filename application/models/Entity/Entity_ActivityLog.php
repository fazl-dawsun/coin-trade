<?php

class Entity_ActivityLog extends Entity{

    function __construct(){
        parent::__construct($this);
    }    

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_ActivityLog");
			
		$ci->Data_ActivityLog->controller = $ci->uri->segment(1);
		$ci->Data_ActivityLog->action = $ci->uri->segment(2);
		$ci->Data_ActivityLog->activity_title = $data["activity_title"];
		$ci->Data_ActivityLog->session_id = session_id();
		$ci->Data_ActivityLog->entity = $data["entity"];
		$ci->Data_ActivityLog->entity_id = $data["entity_id"];		

        $ci->db->set("activity_time", "NOW()", true);	

        return $ci->Data_ActivityLog->insert();
    }

    function modify(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_ActivityLog");

		$ci->Data_ActivityLog->activity_log_id = $data["activity_log_id"];
		$ci->Data_ActivityLog->controller = $data["controller"];
		$ci->Data_ActivityLog->action = $data["action"];
		$ci->Data_ActivityLog->activity_title = $data["activity_title"];
		$ci->Data_ActivityLog->session_id = $data["session_id"];
		$ci->Data_ActivityLog->entity = $data["entity"];
		$ci->Data_ActivityLog->entity_id = $data["entity_id"];
			
        $ci->db->set("activity_time", "NOW()", true);

        return $ci->Data_ActivityLog->update();
    }

    function remove($activity_log_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_ActivityLog");

        return $ci->Data_ActivityLog->delete("activity_log", array("activity_log_id"=>$activity_log_id));
    }

    function fetch($activity_log_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("activity_log")->where("activity_log_id", $activity_log_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("activity_log");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    
}       
