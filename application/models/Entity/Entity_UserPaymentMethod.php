<?php

class Entity_UserPaymentMethod extends Entity{

    function __construct(){
        parent::__contruct($this);
    }    

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_UserPaymentMethod");

        $ci->Data_UserPaymentMethod->user_id = $data["user_id"];
		$ci->Data_UserPaymentMethod->method_name = $data["method_name"];
		$ci->Data_UserPaymentMethod->account_number = $data["account_number"];
        $ci->Data_UserPaymentMethod->bank_name = $data["bank_name"];			

        if($data["method_name"] == "bank"){			
			$ci->Data_UserPaymentMethod->account_title = $data["account_title"];
			
			$ci->Data_UserPaymentMethod->bank_address = $data["bank_address"];
			$ci->Data_UserPaymentMethod->branch_code = $data["branch_code"];			
        }
        else if($data["method_name"] == "mobile"){
            $ci->Data_UserPaymentMethod->cnic = $data["cnic"];
        }    
			

        return $ci->Data_UserPaymentMethod->insert();
    }    

    function modify(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_UserPaymentMethod");		

        $ci->Data_UserPaymentMethod->user_id = $data["user_id"];
        
		$ci->Data_UserPaymentMethod->method_name = $data["method_name"];
		$ci->Data_UserPaymentMethod->account_number = $data["account_number"];
        $ci->Data_UserPaymentMethod->bank_name = $data["bank_name"];			

        if($data["method_name"] == "bank"){			
			$ci->Data_UserPaymentMethod->account_title = $data["account_title"];			
			$ci->Data_UserPaymentMethod->bank_address = $data["bank_address"];
			$ci->Data_UserPaymentMethod->branch_code = $data["branch_code"];			
        }
        else if($data["method_name"] == "mobile"){
            $ci->Data_UserPaymentMethod->cnic = $data["cnic"];
        }    

        $this->db->where("user_payment_method_id", $data["user_payment_method_id"]);
        $this->db->update("user_payment_methods", $data);
			
      //  $ci->Data_UserPaymentMethod->user_payment_method_id = $data["user_payment_method_id"];


       // return $ci->Data_UserPaymentMethod->update();
    }

    function remove($user_payment_method_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_UserPaymentMethod");

        return $ci->Data_UserPaymentMethod->delete("user_payment_methods", array("user_payment_method_id"=>$user_payment_method_id));
    }

    function fetch($user_payment_method_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("user_payment_method")->where("user_payment_method_id", $user_payment_method_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("user_payment_methods");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    
}       
