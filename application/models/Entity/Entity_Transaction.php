<?php

class Entity_Transaction extends Entity{

    function __construct(){
        parent::__contruct($this);
    }    

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Transaction");

			$ci->Data_Transaction->amount = $data["amount"];
			$ci->Data_Transaction->user_id = $data["user_id"];
			$ci->Data_Transaction->payment_method = $data["payment_method"];
			$ci->Data_Transaction->instructions = $data["instructions"];
			$ci->Data_Transaction->transaction_type = $data["transaction_type"];
			$ci->Data_Transaction->transaction_status = $data["transaction_status"];
			
			$ci->Data_Transaction->parent_id = $data["parent_id"];
			$ci->Data_Transaction->transaction_time = $data["transaction_time"];
			

        return $ci->Data_Transaction->insert();
    }

    
    function do_transaction($amount, $user_id, $description, $operation,  $instructions, $type, $status = 0, $parent_id = 0, $transaction_time= NULL ){

        $ci = get_main_instance();

        $ci->load->model("Message/Message_Error");



        $amount = abs($amount);

    
        if($operation == "-"){

                $amount *= (-1);
            }

       // $this->db->reset();    

       /*

        $this->db->select("balance")->from("users")->where("user_id", $user_id)->limit(1);
        $user = $this->db->get()->row();

        */

        $query = "SELECT * FROM users WHERE user_id = '{$user_id}' LIMIT 1";
        $user = $this->db->query($query)->row();

        if(is_object($user)){
            $current_balance = $user->balance;
            $new_balance = $current_balance + $amount;

            if($new_balance < 0){
                $ci->Message_Error->trigger("transactions", "do_transaction", "Insufficient balance");    
                return false;
            }

        }
        else {
            
            $ci->Message_Error->trigger("transactions", "do_transaction", "User not found");

            return false;
        }


            if(!$transaction_time){
                $transaction_time = strtotime(date("Y-m-d H:i:s"));
            }            
	
        $this->db->insert("transactions", array(
            "amount" => $amount,
            "user_id" => $user_id,
            "instructions" => $instructions,
            "description" => $description,
            "transaction_type" => $type,
            "transaction_status" => $status,
            "parent_id" => $parent_id,
            "transaction_time" => $transaction_time,
        ));

        $transaction_id = $this->db->insert_id();


        $this->db->update("users", array("balance" => $new_balance), array("user_id"=>$user_id));

        return $transaction_id;
    }


    function modify(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Transaction");

			$ci->Data_Transaction->transaction_id = $data["transaction_id"];
			$ci->Data_Transaction->sender_id = $data["sender_id"];
			$ci->Data_Transaction->sender_object = $data["sender_object"];
			$ci->Data_Transaction->amount = $data["amount"];
			$ci->Data_Transaction->payment_method = $data["payment_method"];
			$ci->Data_Transaction->instructions = $data["instructions"];
			$ci->Data_Transaction->transaction_type = $data["transaction_type"];
			$ci->Data_Transaction->transaction_status = $data["transaction_status"];
			$ci->Data_Transaction->reciever_id = $data["reciever_id"];
			$ci->Data_Transaction->parent_id = $data["parent_id"];
			$ci->Data_Transaction->transaction_time = $data["transaction_time"];
			

        return $ci->Data_Transaction->update();
    }

    function remove($transaction_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Transaction");

        return $ci->Data_Transaction->delete("transactions", array("transaction_id"=>$transaction_id));
    }

    function fetch($transaction_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("transactions")->where("transaction_id", $transaction_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("transactions");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    
}       
