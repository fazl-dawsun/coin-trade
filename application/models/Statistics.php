<?php

class Statistics extends CI_Model{

    function get_users(){

        $this->db->select(" COUNT( user_id ) as counter")->from("users");
        return $this->db->get()->row()->counter;
        
    }

    function get_entrepreneurs(){
        $this->db->select(" COUNT( DISTINCT owner_id ) as counter")->from("projects");
        return $this->db->get()->row()->counter;
        
    }

    function get_investments(){
        $this->db->select(" SUM( required_investment ) as total_investment")->from("projects");
        return $this->db->get()->row()->total_investment;
    }

    function get_funds(){
        $this->db->select(" SUM( amount ) as total_funds")->from("investments");
        return $this->db->get()->row()->total_funds;
    }

    function get_investers(){
        $this->db->select(" COUNT( invester_id ) as total_funds")->from("investments");
        return $this->db->get()->row()->total_funds;
    }

    function get_projects(){
        $this->db->select(" COUNT( project_id ) as total_projects")->from("projects");
        return $this->db->get()->row()->total_projects;
    }
}