<?php

class Data_User extends Data{
    protected $primaryKeys = array("user_id");
    protected $table = "users";
    protected $data = array();

    function __construct(){
        $ci = get_instance();
        $ci->load->model("Message/Message_Error");
        $ci->load->model("Message/Message_Warning");
    }

    function __set($var, $value){

        switch($var){

            case "user_id":
              
            break;

            case "first_name":
            case "last_name":
                if(!$value){
                    $this->Message_Error->trigger($this->table, $var, "Please enter your " . str_replace("_", " ", $var));                     
                }
                preg_match_all("#[^A-Za-z\s\.]#", $value, $matches);                
                if(count($matches[0])>0){
                    $this->Message_Error->trigger($this->table, $var, "Illegal characters found in your " . str_replace("_", " ", $var)
                                 . " ( " . implode(", ", $matches[0]) . " )");
                }
            break;


            case "email":
                if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
                    $this->Message_Error->trigger($this->table, "email", "Please enter valid email address");
                }
            break;

            case "phone":
                if( ! $value  ){
                    $this->Message_Error->trigger($this->table, "phone", "Please enter your phone number");
                    return ;
                }
                preg_match_all("#[^0-9\+\s\-_]#", $value, $matches);                
                if(count($matches[0])>0){
                    $this->Message_Error->trigger($this->table, $var, "Illegal characters found in your phone number ( " . implode(", ", $matches[0]) . " )");
                }
            break;

            case "date_of_birth":

            break;

            case "city":
            case "state":
            case "country":
               
            break;

            case "password":
                if( strlen($value) < 6 ){
                    $this->Message_Error->trigger($this->table, "password", "Please enter password of minimum 6 characters");
                    return ;
                }
                else if(preg_match("[\s]", $value)){
                    $this->Message_Error->trigger($this->table, "password", "Space is not allowed in password");
                    return ;
                }
                else {
                    $value = md5($value);                
                }      
            break;            

            case "balance":

            break;
            
                  case "imagedata":

            break;
                
            case "profile_image":

            break;

            case "gender":

            break;

            case "session_key":

            break;

             case "access_token":

            break;

             case "expiresIn":

            break;

             case "sig":

            break;

            case "secret":

            break;
            
             case "user_fbid":

            break;

            case "referrer_id":

            break;

            case "ip_address":

            break;


            default:
                return false;
            break;

        }


        $this->data[$var] = $value;


    }

    

    
}