<?php

class Event extends CI_Model{

     const USER_POSTED_PROJECT = "user_posted_project";
     const USER_UPDATED_PROFILE = "user_updated_profile";
     const USER_INVESTED = "user_invested";

     const PROJECT_MODIFIED = "project_modified";
     const PROJECT_INVEST = "project_invest";
     const PROJECT_INVEST_FEEDBACK = "project_invest_feedback";
     const PROJECT_STATUS_CHANGE  = "project_status_change";

     public static $titles = array(
            self::USER_POSTED_PROJECT => "User has posted project",
             self::USER_UPDATED_PROFILE => "User has updatd profile",
             self::USER_INVESTED => "User have invested in project",

             self::PROJECT_MODIFIED => "Project is modified",
             self::PROJECT_INVEST => "Project get investment",
             self::PROJECT_INVEST_FEEDBACK => "Project Reward feedback posted ");

     function __construct(){
         $titles = array(
             self::USER_POSTED_PROJECT => "User has posted project",
             self::USER_UPDATED_PROFILE => "User has updatd profile",
             self::USER_INVESTED => "User have invested in project",

             self::PROJECT_MODIFIED => "Project is modified",
             self::PROJECT_INVEST => "Project get investment",
             self::PROJECT_INVEST_FEEDBACK => "Project Reward feedback posted ",

         );
     }

     function trigger( $entity, $entity_id, $event_key, array $data = array()){
          $this->db->select("*")->from("followings")->where(array(
              "entity" => $entity,
              "entity_id" => $entity_id
          ));

          $followings = $this->db->get()->result();

/*
          echo '<pre>'; print_r($followings); print_r(array(
              "entity" => $entity,
              "entity_id" => $entity_id
          )); echo '</prE>'; exit;
*/

          $receiver_ids = array();

          for($i=0, $count = count($followings); $i < $count; $i++ ){
              $this->db->set("notification_date", "NOW()", false);
              $this->db->insert("notifications", array(
                  "notification_entity" => $followings[$i]->entity,
                  "notification_entity_id" => $followings[$i]->entity_id,
                  "notification_receiver_id" => $followings[$i]->user_id,
                  "notification_event_key" => $event_key,
                  "notification_data" => json_encode($data)
              ));
              $receiver_ids[] = $followings[$i]->user_id;
          }

          switch($event_key){
              case self::PROJECT_INVEST:
              case self::PROJECT_INVEST_FEEDBACK:
              case self::PROJECT_STATUS_CHANGE:
                 $this->db->select("*")->from("projects")->where("project_id", $entity_id);
                 $project = $this->db->get()->row();
                 $this->db->set("notification_date", "NOW()", false);
                 $this->db->insert("notifications", array(
                    "notification_entity" => "projects",
                    "notification_entity_id" => $project->project_id,
                    "notification_receiver_id" => $project->owner_id,
                    "notification_event_key" => $event_key,
                    "notification_data" => json_encode($data)
                ));
                $receiver_ids[] = $project->owner_id;
              break;
          }

          for($i=0, $count = count($receiver_ids); $i < $count; $i++){
              $this->send_email( $receiver_ids[$i] );
          }
     }

     function get_notifications(array $cond = array()){

        $this->db->select("*")->from("notifications");

        if(count( $cond ) > 0){
            $this->db->where($cond);
        }

        $notifications = $this->db->get()->result_array();

        for($i=0, $count = count($notifications); $i < $count ; $i++){

            switch($notifications[$i]["notification_entity"]){

                case "projects":
                    $this->db->select("*")->from("projects")->where("project_id", $notifications[$i]["notification_entity_id"]);
                    $project = $this->db->get()->row();
                    $notifications[$i]["entity_title"] = $project->project_title;
                    $notifications[$i]["url"] = get_route("project", "detail", array($notifications[$i]["notification_entity_id"]));

                break;

                case "users":
                    $this->db->select("*")->from("users")->where("user_id", $notifications[$i]["notification_entity_id"]);
                    $user = $this->db->get()->row();
                    $notifications[$i]["entity_title"] = $user->user_name;
                    $notifications[$i]["url"] = get_route("user", "view_profile", array($notifications[$i]["notification_entity_id"]));

                break;
            }

            $notifications[$i]["notification_title"] = Event::$titles[ $notifications[$i]["notification_event_key"] ];

        }

        return $notifications;

     }

     function get_notification_count_by( array $filter = array() ){
          $this->db->select("COUNT(*) as rows_count")->from("notifications")->where($filter);
          return $this->db->get()->row()->rows_count;
     }

     function mark_as_seen( $notification_id ){
         $this->db->where("notification_id", $notification_id);
         $this->db->set("notification_seen_time", "NOW()", false);
         return $this->db->update("notifications", array( "is_seen" => 1));
     }

     function send_email($receiver_id){

            $ci = get_main_instance();

    


            $notifications = $this->get_notifications(array(
                "notification_receiver_id" => $receiver_id
            ));

            $this->db->select("*")->from("users")->where("user_id", $receiver_id);

            $receiver = $this->db->get()->row();

            $content = "You have notification" ;

            ob_start();

            $this->load->view("notification/my.phtml", array(
               // "title" => "My Notifications",
                "my_notifications" => $notifications
            ));

            $content .= ob_get_contents();
            @ob_end_clean();

            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';

            // Additional headers
            $headers[] = 'To: ' . ucwords($receiver->first_name . " " . $receiver->last_name)  . ' <' . $receiver->email . '>';
            $headers[] = 'From: '.get_site_name().' <'.get_site_email().'>';

            print_r(mail($receiver->email, "Notification from " . get_site_name(), $content, implode("\r\n", $headers)));


     }


}
