<?php

class Message extends CI_Model{

    protected static $messages;

    function log($type, $entity, $source, $message){
        if(@$this->type) $type = $this->type;
        static::$messages[$type][$entity][$source][] = $message; 
    }

    function get_by_type($type){
        return static::$messages[$type];
    }

    function get_by_entity($entity){
        $entity_msg = array();
        foreach(static::$messages[$type] as $type_index => $type){
            foreach($type as $entity_index => $entity){
                if($entity_index == $entity){
                    $entity_msg[$type_index] = $entity[$source];
                }
            }
        }

        return $entity_msg;
    }        
}