<?php

class Data extends CI_Model{

    function insert($table = NULL, $data = NULL){
        if( !$table ) $table = $this->table;
        if( !$data ) $data = $this->data;   

        if(is_object($this->Message_Error)){
            $errors = $this->Message_Error->get_by_entity($this->table);
            if(count($errors) > 0) return false;
        }

        for($i=0, $count = count($this->primaryKeys); $i < $count; $i++){
            unset($data[$this->primaryKeys[$i]]);
        }

        if(isset($_GET["debug"])  && $_GET["debug"] == "sql_insert" && isset($_GET["table"]) && $_GET["table"] == $table){
            echo json_encode(array(
                "table" => $table,
                "data" => $data
            ));
            exit;
        }

        

        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update($table = NULL, $data = NULL, $cond = array()){

   
    
        if( !$table ) $table = $this->table;
        if( !$data ) $data = $this->data;

        if(is_object($this->Message_Error)){
            $errors = $this->Message_Error->get_by_entity($this->table);
            if(count($errors) > 0) return false;
        }

        if(count($cond) == 0 && isset($this->primaryKeys) && is_array($this->primaryKeys) && count($this->primaryKeys) > 0){
            foreach($data as $index => $value){
                if(in_array($index, $this->primaryKeys)){

                    $cond[$index] = (int) $value;
                    unset($data[$index]);
                }
            }
        }

        //if(count($cond) > 0) $this->db->where($cond);

        if(isset($_GET["debug"])  && $_GET["debug"] == "sql_update" && isset($_GET["table"]) && $_GET["table"] == $table){

            $where = $set = null;

            foreach($data as $column => $value){
                $set[] = " {$column} = '{$value}' " ;
            }

            foreach($cond as $column => $value){
                $where[] = " {$column} = {$value} " ;
            }

           
            

       
        }

        $this->db->where($cond);
        $updated =$this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    function delete($table = NULL, $cond = array()){

        if( !$table ) $table = $this->table;

        if(count($cond) == 0 && isset($this->primaryKeys) && is_array($this->primaryKeys) && count($this->primaryKeys) > 0){
            foreach($this->data as $index => $value){
                if(in_array($index, $this->primaryKeys)){
                    $cond[$index] = $value;
                    unset($this->data[$index]);
                }
            }
        }

        if(count($cond) > 0) $this->db->where($cond);

        $this->db->delete($table);
        return $this->db->affected_rows();
    }


}
