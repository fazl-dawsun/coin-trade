<?php

class Entity_Draw extends Entity{

    function __construct(){
        parent::__contruct($this);
    }    

    function add(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Draw");

        $draw_time = $data["draw_time"]; 

        
		$ci->Data_Draw->draw_time = $data["draw_time"];
		$ci->Data_Draw->creation_date = date("Y-m-d H:i:s");        
		$ci->Data_Draw->create_time = time();

        if(strtotime($data["draw_time"]) > time()){
            $this->db->set("lucky_number", null);
            $this->db->set("draw_number", null);
        }

        /*
        if(isset($data["lucky_number"])){
            //$ci->Data_Draw->lucky_number = $data["lucky_number"];
            $this->db->set("lucky_number", $data["lucky_number"]);
        }
        else {
            $ci->Data_Draw->lucky_number = null;
        }*/

        //$ci->Data_Draw->draw_number = null;
			
        if(isset($data["admin_override"])){
           $ci->Data_Draw->admin_override = $data["admin_override"];
        }		


        return $ci->Data_Draw->insert();
    }

    function run(array $data){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Draw");        

        $lucky_numbers = $draw_number = $draw_time = NULL;



		$ci->Data_Draw->draw_id = $data["draw_id"];         
        
        $query = $this->db->query("SELECT draw_number, draw_time FROM draws WHERE draw_id < {$data['draw_id']} ORDER BY draw_id DESC");
        $row = $query->row();                

        if(is_object($row)){
            $draw_number = $row->draw_number;
            $draw_time = $row->draw_time;            
        }           

        $this->db->select("*")->from("draws")->where("draw_id", $data["draw_id"]);
        $current_draw = $this->db->get()->row();

        $now = time();      
        $now += PRE_DRAW_INTERVAL;         

        if(strtotime($current_draw->draw_time) > $now){
            /*file_put_contents(__DIR__ . "/draw_time.log", print_r(array(
                "draw_time" => $current_draw->draw_time,
                "now" => date("Y-m-d H:i:s", $now),

            ), true));
            */
            return  ;
        } 

        if($current_draw->lucky_number === 0 || ($current_draw->lucky_number >= 1 && $current_draw->lucky_number <= 9) ){
            $lucky_number = $current_draw->lucky_number;
           // header("Content-Type: application/json"); echo json_encode(array("status"=> "admin_override", "current_draw"=>$current_draw, "lucky_numbers"=> $lucky_numbers, "lucky_number"=> $lucky_number));             exit;
        }
        else {

            $lucky_numbers = $this->get_luck_number_for_draw($current_draw->draw_time);

            /*

            foreach($lucky_numbers as $number => $coins){
                $lucky_number = (int) $number;
                break;
            } 
            */

            $lucky_number = (int) $this->get_random_lucky_number($lucky_numbers);           

        }


        $this->db->where("draw_id", $data["draw_id"]);
        $this->db->update("draws", array(
            "lucky_number" => $lucky_number,
            "draw_number" => get_draw_number($draw_number, $draw_time)
        ));
        

    }

    function distripute_prize_money($draw_id){

        $this->db->select("*")->from("draws")->where("draw_id", $draw_id);
        $current_draw = $this->db->get()->row();

        $this->db->select("*")->from("participants")->where(array(
            /*"lucky_number" => $lucky_number,*/
            "draw_time" => $current_draw->draw_time
        ))->join("users", "participants.user_id = users.user_id");

        $query = $this->db->get();

        $participants = $query->result();

        $current_draw_time = date("F d, Y H:i A", strtotime($current_draw->draw_time));

        $count_participants = count($participants);

        file_put_contents(__DIR__ . "/participants.log" , print_r($participants, true));

        if($count_participants > 0){

            $ci = get_main_instance();

            $ci->load->library("my_email", null, "my_email");
            $ci->my_email->from('cointrade@trailweb.site', get_site_name());         
                
            $ci->my_email->subject("Congratulations! You won lucky draw at " . get_site_name());        

           // $this->db->trans_begin();

            for($i=0; $i < $count_participants; $i++){
                if($participants[$i]->lucky_number == $current_draw->lucky_number){
                    $reward = $participants[$i]->coins * 9;
                    $this->db->where("participant_id", $participants[$i]->participant_id);
                    $this->db->update("participants", array(
                        "winner" => 1,
                        "reward" => $reward 
                    ));
                    //$this->db->set("transaction_time", "NOW()", false);
                    $this->db->insert("transactions", array(
                        "user_id" => $participants[$i]->user_id,
                        "amount" => $reward,
                        "transaction_type" => "draw winner",
                        "instructions" => "Transfering winning reward from {$current_draw->draw_time} having lucky_number: {$current_draw->lucky_number}  ",
                        "transaction_status" => 1,
                        "transaction_time" => date("Y-m-d H:i:s")                    
                    ));

                    $this->db->query("UPDATE users SET balance = balance + {$reward} WHERE user_id = '{$participants[$i]->user_id}'");

                    $this->db->select("*")->from("users")->where("user_id", $participants[$i]->user_id);

                    $user = $this->db->get()->row();

                    $ci->my_email->to($user->email);
                    $ci->my_email->message("Congratulation! You won lucky draw at {$current_draw_time}.<br><div><strong>Reward: </strong>{$reward}</div>");

                    $ci->my_email->send();
                }   
                $this->db->where("participant_id", $participants[$i]->participant_id);
                $this->db->update("participants", array(
                    "draw_id" => $current_draw->draw_id
                ));

                
            }   
            /*
            if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }
                else{
                        $this->db->trans_commit();
                }
            */

          }   

    }

    function run_in_queue(){
        $query = $this->db->query("SELECT * FROM draws WHERE draw_number IS NULL OR draw_number = 0");
        $draws = $query->result();

      //  echo '<pre>'; print_r($draws); echo '</pre>'; exit;

        for($i=0, $count = count($draws); $i < $count; $i++){
            $this->run(array(
                "draw_id" => $draws[$i]->draw_id
            ));
        }

        $this->db->select("COUNT(*) as num_rec")->from("draws");
        $num_of_recs = $this->db->get()->row()->num_rec;

      //  echo "rows: " . $num_of_recs; exit; 

        if($num_of_recs == 0){
            $draw_time_tmp = date("Y-m-d H:00:00");         
            $temp = $draw_time = strtotime($draw_time_tmp);
            $draw_time += 60 * 60;             
       //     echo date("Y-m-d H:i:s", $draw_time); exit;
            $this->add(array(
                "draw_time" => date("Y-m-d H:i:s", $draw_time)
            ));
            return ;
        }

        /*
        
        $query = $this->db->query("SELECT MAX(draw_time) as last_draw_time FROM draws");

        $row = $query->row();

        */

        $this->db->select("*")->from("draws")->order_by("draw_id", "desc")->limit(1);
        $draw = $this->db->get()->row();

        $draw_time = strtotime($draw->draw_time);

        $draw_time += DRAW_INTERVAL;      

        while( $draw_time <= time() + DRAW_INTERVAL ){
            /*
            $draw_id = $this->add(array(
                "draw_time" => date("Y-m-d H:i:s", $draw_time)
            ));

            */

            $this->db->insert("draws", array(
                  "draw_time" => date("Y-m-d H:i:s", $draw_time),
                  "creation_date" => date("Y-m-d H:i:s"),  
                  "create_time" => time()  
            ));

            $draw_id = $this->db->insert_id();                    
                         
            $this->run(array(
                "draw_id" => $draw_id
            ));

            $this->db->select("*")->from("draws")->order_by("draw_id", "desc")->limit(1);
            $draw = $this->db->get()->row();

            $draw_time = strtotime($draw->draw_time);

            $draw_time += DRAW_INTERVAL;

        } ;

        /*$last_draw_time += 60 * 20;
            $draw_id = $this->add(array(
                "draw_time" => date("Y-m-d H:i:s", $last_draw_time),
                "admin_override" => 3
            ));
*/
       $this->db->query("COMMIT");     

      //      echo date("Y-m-d H:i:s", $last_draw_time); exit;    
  }

    function remove($draw_id){

        $ci = get_main_instance();

        $ci->load->model("Data/Data_Draw");

        return $ci->Data_Draw->delete("draws", array("draw_id"=>$draw_id));
    }

    function fetch($draw_id){
        $ci = get_main_instance();
        $this->db->select("*")->from("draws")->where("draw_id", $draw_id);
        return $this->db->get()->row();
    }

    function fetch_bulk(array $cond = array()){
        $ci = get_main_instance();
        $this->db->select("*")->from("draws");
        if( count($cond) > 0){
            $this->db->where($cond);
        }    
        return $this->db->get()->result();
    }    

    function get_luck_number_for_draw($draw_time = NULL){

        $draw_time = $this->db->select("lucky_number, draw_time")->from("draws")->where("draw_time", $draw_time);
        $draw = $this->db->get()->row();

        if(is_object($draw)){
            $draw_time = $draw->draw_time;
            $lucky_number = $draw->lucky_number;
            /*
            if(in_array($lucky_number, array(0,1,2,3,4,5,6,7,8,9))){
                $this->db->select("SUM(IFNULL(coins, 0)) as num_of_participants")->from("participants")->where("lucky_number", $lucky_number);
                $this->db->where("draw_time", $draw_time);
                $result = $this->db->get();
                $number = $result->row();          
            
                return array( $lucky_number => $number->num_of_participants ); 
            }
            */
        }
        else {
            $this->db->select("MAX(draw_time) as draw_time")->from("draws");
            $draw = $this->db->get()->row();
            $draw_time = $draw->draw_time;
            $tmp_time = strtotime($draw_time);
            $draw_time = date("Y-m-d H:i:s", $tmp_time + DRAW_INTERVAL);
        }

        for($i=0; $i <= 9; $i++ ){           

           
                $this->db->select("SUM(IFNULL(coins, 0)) as num_of_participants")->from("participants")->where("lucky_number", $i);
                $this->db->where("draw_time", $draw_time);
                $result = $this->db->get();
                $number = $result->row();          
            
            $lucky_numbers[$i] = $number->num_of_participants; 

        }

        asort($lucky_numbers, SORT_NUMERIC);        
       
        return $lucky_numbers;
    }

    function get_random_lucky_number(array $lucky_numbers){
        $opponents = array();
        foreach($lucky_numbers as $number => $coins){
            $selected_coins = $coins;
            break;
        }

        foreach($lucky_numbers as $number => $coins){
            if($selected_coins == $coins) $opponents[] = $number;
        }

        // If you want draw result with max profit use $opponents
/*
        $index = rand(0, count($opponents) - 1);

     
        return $opponents[$index];
*/


        //if you want to be fair use $lucky_numbers

        $index = rand(0, 9);


 //       file_put_contents(__DIR__ . "/lucky_number.log", print_r($lucky_numbers, true));


        return $index ;


        
    }
}