<?php

class Message_Error extends Message{
    //protected static $messages;
    protected $type = "error";

    function trigger($entity, $source, $message){
        parent::log($this->type, $entity, $source, $message);        
      //  echo '<pre>'; 
        //print_r(array("entity"=>$entity, "source"=> $source, "message"=>$message)); 
    }

    function get_all(){
        return parent::get_by_type($this->type);    
    }

    function get_by_entity($entity){
       $errors = $this->get_all();       
       return isset($errors[$entity]) ? @$errors[$entity] : array();
    } 
} 