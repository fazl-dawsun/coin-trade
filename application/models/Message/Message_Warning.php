<?php

class Message_Warning extends Message{
    protected $type = "warning";

    function trigger($entity, $source, $message){
        parent::log($this->type, $entity, $source, $message);
    }

    function get_all(){
        return parent::get_by_type($this->type);    
    }

    function get_by_entity($entity){
       $errors = $this->get_all();
       return $errors[$entity];
    } 
} 